<?php

return [
    'catalog' => [
        'offers' => [
            'base_uri' => env('CATALOG_OFFERS_SERVICE_HOST') . "/api/v1",
        ],
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'logistic' => [
        'logistic' => [
            'base_uri' => env('LOGISTIC_LOGISTIC_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'customers' => [
        'customers' => [
            'base_uri' => env('CUSTOMERS_CUSTOMERS_SERVICE_HOST') . '/api/v1',
        ],
    ],
];
