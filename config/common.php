<?php

return [
    'payment_duration' => 60,
    'order_duration' => 720,
    'time_for_refund' => 20160,
    'order_number_prefix' => env('ORDER_NUMBER_PREFIX', ''),
];
