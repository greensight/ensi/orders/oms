<?php

namespace Database\Seeders;

use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\CommitOrderAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\CommitData;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\DeliveryData;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\ShipmentData;
use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\Timeslot;
use App\Http\ApiV1\OpenApiGenerated\Enums\CountryCodeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\PaginationTypeEnum as CustomersPaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination as CustomersRequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\PaginationTypeEnum as OffersPaginationTypeEnum;
use Ensi\OffersClient\Dto\RequestBodyPagination as OffersRequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Exception;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;

class OrdersSeeder extends Seeder
{
    protected const ORDERS_COUNT = 15;

    /** @var Collection|Offer[] */
    protected Collection $offers;
    /** @var Collection<Product> */
    protected Collection $products;
    protected Collection $customers;
    protected Collection $deliveryServices;

    public function __construct(
        protected readonly CommitOrderAction $commitOrderAction,
        protected readonly Generator $faker,
        protected readonly OffersApi $offersApi,
        protected readonly ProductsApi $productsApi,
        protected readonly CustomersApi $customersApi,
        protected readonly DeliveryServicesApi $deliveryServicesApi,
    ) {
        $this->loadOffers();
        $this->loadProducts();
        $this->loadCustomers();
        $this->loadDeliveryServices();
    }

    public function run(): void
    {
        for ($i = 0; $i < self::ORDERS_COUNT; $i++) {
            try {
                $commitData = $this->makeCommitData();
                $order = $this->commitOrderAction->execute($commitData);

                if ($this->faker->boolean()) {
                    $order->payment_status = PaymentStatusEnum::PAID;
                    $order->payed_at = Date::now();
                    $order->payed_price = $order->price;
                    $order->status = OrderStatusEnum::NEW;
                    $order->save();

                    if ($this->faker->boolean()) {
                        $order->status = OrderStatusEnum::APPROVED;
                        $order->save();
                    }

                    try {
                        if ($order->status == OrderStatusEnum::APPROVED) {
                            if ($this->faker->boolean()) {
                                $order->status = OrderStatusEnum::DONE;
                            }
                        } else {
                            $order->status = $this->faker->randomElement([
                                OrderStatusEnum::NEW,
                                OrderStatusEnum::APPROVED,
                                OrderStatusEnum::CANCELED,
                            ]);
                        }
                        $order->save();
                    } catch (Exception $e) {
                        print $e->getMessage();
                    }
                }
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
    }

    protected function loadOffers(): void
    {
        $request = new SearchOffersRequest();
        $request->setPagination((new OffersRequestBodyPagination())
            ->setLimit(100)
            ->setType(OffersPaginationTypeEnum::CURSOR));
        $this->offers = collect($this->offersApi->searchOffers($request)->getData())
            ->keyBy(fn (Offer $offer) => $offer->getId());
    }

    protected function loadProducts(): void
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $this->offers->pluck('product_id'), 'allow_publish' => true]);
        $request->setPagination((new PimRequestBodyPagination())
            ->setLimit($this->offers->keys()->count())
            ->setType(PimPaginationTypeEnum::CURSOR));
        $this->products = collect($this->productsApi->searchProducts($request)->getData())
            ->keyBy(fn (Product $product) => $product->getId());
        $this->offers = $this->offers->filter(fn (Offer $offer) => $this->products->has($offer->getProductId()));
    }

    protected function loadCustomers(): void
    {
        $request = new SearchCustomersRequest();
        $request->setPagination((new CustomersRequestBodyPagination())
            ->setLimit(100)
            ->setType(CustomersPaginationTypeEnum::CURSOR));
        $this->customers = collect($this->customersApi->searchCustomers($request)->getData())
            ->keyBy(fn (Customer $customer) => $customer->getId());
    }

    protected function loadDeliveryServices(): void
    {
        $this->deliveryServices = collect($this->deliveryServicesApi->searchDeliveryServices(new SearchDeliveryServicesRequest())->getData())
            ->keyBy(fn (DeliveryService $deliveryService) => $deliveryService->getId());
    }

    protected function makeCommitData(): CommitData
    {
        $commitData = new CommitData();
        /** @var Customer $customer */
        $customer = $this->faker->randomElement($this->customers) ?? new Customer([
            'id' => $this->faker->numberBetween(1),
            'email' => $this->faker->email(),
            'full_name' => $this->faker->name(),
            'phone' => $this->faker->numerify('+7##########'),
        ]);
        $commitData->customerId = $customer->getId();
        $commitData->customerEmail = $customer->getEmail();
        $commitData->clientComment = $this->faker->optional()->text();

        $commitData->source = $this->faker->randomElement(OrderSourceEnum::cases());
        $commitData->paymentSystemId = $this->faker->randomElement(PaymentSystemEnum::cases());
        $commitData->spentBonus = null;
        $commitData->addedBonus = null;
        $commitData->promoCode = null;

        $commitData->deliveryService = $this->faker->randomElement($this->deliveryServices->keys());
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $commitData->deliveryMethod = $deliveryMethod;
        if ($deliveryMethod == DeliveryMethodEnum::PICKUP) {
            $commitData->deliveryPointId = $this->faker->numberBetween(1);
            $commitData->deliveryAddress = null;
        } else {
            $commitData->deliveryPointId = null;
            $commitData->deliveryAddress = new DeliveryAddress([
                'address_string' => $this->faker->address(),
                'country_code' => $this->faker->randomElement(CountryCodeEnum::cases()),
                'post_index' => $this->faker->postcode(),
                'region' => $this->faker->city(),
                'region_guid' => $this->faker->uuid(),
                'area' => $this->faker->city(),
                'area_guid' => $this->faker->uuid(),
                'city' => $this->faker->city(),
                'city_guid' => $this->faker->uuid(),
                'street' => $this->faker->streetAddress(),
                'house' => $this->faker->buildingNumber(),
                'block' => $this->faker->numerify('##'),
                'flat' => $this->faker->numerify('##'),
                'floor' => $this->faker->numerify('##'),
                'porch' => $this->faker->numerify('##'),
                'intercom' => $this->faker->numerify('##-##'),
                'geo_lat' => (string)$this->faker->latitude(),
                'geo_lon' => (string)$this->faker->longitude(),
            ]);
        }
        $commitData->deliveryComment = $this->faker->optional()->text();
        $deliveryPrice = $this->faker->numberBetween(0, 200);
        $commitData->deliveryCost = $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500);
        $commitData->deliveryPrice = $deliveryPrice;

        $commitData->receiverName = $customer->getFullName();
        $commitData->receiverPhone = $customer->getPhone();
        $commitData->receiverEmail = $customer->getEmail();

        $commitData->items = $this->makeItems();
        $commitData->deliveries = $this->makeDeliveries($commitData->items);

        return $commitData;
    }

    protected function makeItems(): Collection
    {
        $offers = $this->faker->randomElements(
            $this->offers->all(),
            $this->faker->numberBetween(1, $this->offers->count())
        );
        $items = collect();

        /** @var Offer $offer */
        foreach ($offers as $offer) {
            /** @var Product $product */
            $product = $this->products->get($offer->getProductId());

            $orderItemData = new OrderItemData();
            $items->push($orderItemData);
            $orderItemData->offerId = $offer->getId();
            $orderItemData->costPerOne = $offer->getPrice();
            $orderItemData->pricePerOne = $offer->getPrice();
            $orderItemData->qty = $this->faker->numberBetween(
                $product->getOrderMinvol() > 0 ? $product->getOrderMinvol() : 1,
                $product->getOrderMinvol() > 0 ? $product->getOrderMinvol() + 5 : 5
            );
        }

        return $items;
    }

    protected function makeDeliveries(Collection $items): Collection
    {
        $deliveries = collect();

        $deliveriesCount = $this->faker->numberBetween(1, $items->count());
        for ($i = 0; $i < $deliveriesCount; $i++) {
            $deliveryData = new DeliveryData();
            $deliveries->push($deliveryData);
            $deliveryData->date = Date::make($this->faker->date());
            $deliveryData->timeslot = new Timeslot([
                'id' => $this->faker->uuid(),
                'from' => $this->faker->time('H:i'),
                'to' => $this->faker->time('H:i'),
            ]);
            $deliveryData->cost = $this->faker->numberBetween(0, 200);

            $deliveryData->shipments = $this->makeShipments($deliveriesCount, $i, $items);
        }

        return $deliveries;
    }

    protected function makeShipments(int $deliveriesCount, int $deliveryNumber, Collection $items): array
    {
        $shipments = [];
        $shipmentsItems = $items->chunk((int)floor($items->count() / $deliveriesCount))[$deliveryNumber];
        $shipmentsCount = $this->faker->numberBetween(1, $shipmentsItems->count());
        $itemsByShipment = $items->chunk($shipmentsCount);

        for ($i = 0; $i < count($itemsByShipment); $i++) {
            /** @var Collection $shipmentItems */
            $shipmentItems = $itemsByShipment[$i];
            $shipmentData = new ShipmentData();
            $shipmentData->storeId = $this->faker->numberBetween(1);
            $shipmentData->items = $shipmentItems->values()->all();
            $shipments[] = $shipmentData;
        }

        return $shipments;
    }
}
