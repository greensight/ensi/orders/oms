<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public const DEFAULT_SELLER_ID = 1;

    public function up(): void
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('offer_storage_address');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('seller_id');
        });
    }

    public function down(): void
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->string('offer_storage_address')->nullable();
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->unsignedBigInteger('seller_id')->nullable();
        });
        DB::table('shipments')->update(['seller_id' => self::DEFAULT_SELLER_ID]);
        Schema::table('shipments', function (Blueprint $table) {
            $table->unsignedBigInteger('seller_id')->nullable(false)->change();
        });
    }
};
