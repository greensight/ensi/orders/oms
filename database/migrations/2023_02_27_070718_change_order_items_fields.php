<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->decimal('product_weight', 18, 4)->nullable()->change();
            $table->decimal('product_weight_gross', 18, 4)->nullable()->change();
            $table->decimal('product_width', 18, 4)->nullable()->change();
            $table->decimal('product_height', 18, 4)->nullable()->change();
            $table->decimal('product_length', 18, 4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->decimal('product_weight', 18, 4)->nullable(false)->change();
            $table->decimal('product_weight_gross', 18, 4)->nullable(false)->change();
            $table->decimal('product_width', 18, 4)->nullable(false)->change();
            $table->decimal('product_height', 18, 4)->nullable(false)->change();
            $table->decimal('product_length', 18, 4)->nullable(false)->change();
        });
    }
};
