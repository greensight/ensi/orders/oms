<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->integer('width')->default(0)->change();
            $table->integer('height')->default(0)->change();
            $table->integer('length')->default(0)->change();
            $table->integer('weight')->default(0)->change();
        });
    }

    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->decimal('width', 18, 4)->default(0.0)->change();
            $table->decimal('height', 18, 4)->default(0.0)->change();
            $table->decimal('length', 18, 4)->default(0.0)->change();
            $table->decimal('weight', 18, 4)->default(0.0)->change();
        });
    }
};
