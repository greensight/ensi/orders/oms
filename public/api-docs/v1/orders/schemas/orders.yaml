OrderReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: ID заказа
      example: 1
    number:
      type: string
      description: номер заказа
      example: "1000001"
    customer_id:
      type: integer
      description: id покупателя
      example: 5
    customer_email:
      type: string
      description: почта покупателя
      example: 'example@domail.ru'
    cost:
      type: integer
      description: стоимость (без учета скидки) (рассчитывается автоматически) в коп.
      example: 10000
    price:
      type: integer
      description: стоимость (с учетом скидок) (рассчитывается автоматически) в коп.
      example: 10000
    spent_bonus:
      type: integer
      description: списано бонусов
      example: 0
    added_bonus:
      type: integer
      description: начислено бонусов
      example: 0
    promo_code:
      type: string
      description: Использованный промокод
      example: 'promo_code'
      nullable: true
    status_at:
      type: string
      format: date-time
      description: дата установки статуса заказа
      example: "2021-06-11T11:27:10.000000Z"
    source:
      type: integer
      description: источник заказа из OrderSourceEnum
      example: 1
    payment_status:
      type: integer
      description: статус оплаты из PaymentStatusEnum
      example: 1
    payment_status_at:
      type: string
      format: date-time
      description: дата установки статуса оплаты
      example: "2021-06-11T11:27:10.000000Z"
    payed_at:
      type: string
      format: date-time
      description: Дата оплаты
      example: "2020-12-15T15:21:34.000000Z"
      nullable: true
    payed_price:
      type: integer
      description: Сумма оплаты
      nullable: true
    payment_expires_at:
      type: string
      format: date-time
      description: Дата, до которой нужно провести оплату
      nullable: true
      example: "2020-12-15T15:21:34.000000Z"
    payment_method:
      type: integer
      description: метод оплаты из PaymentMethodEnum
    payment_system:
      allOf:
        - type: integer
        - $ref: '../../payment_systems/enums/payment_system_enum.yaml'
    payment_link:
      type: string
      description: Ссылка для оплаты во внещней системе
      nullable: true
    payment_external_id:
      type: string
      description: ID оплаты во внешней системе
      nullable: true
    payment_data:
      $ref: '#/OrderPaymentData'
    is_problem_at:
      type: string
      format: date-time
      description: дата установки флага проблемного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_editable:
      type: boolean
      description: флаг, что заказ может быть изменён
    is_changed:
      type: boolean
      description: флаг, что заказ был изменён
    is_expired:
      type: boolean
      description: флаг, что заказ просроченный
    is_expired_at:
      type: string
      format: date-time
      description: дата установки флага просроченного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_return:
      type: boolean
      description: флаг, что заказ возвращен
    is_return_at:
      type: string
      format: date-time
      description: дата установки флага возвращенного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    is_partial_return:
      type: boolean
      description: флаг, что заказ частично возвращен
    is_partial_return_at:
      type: string
      format: date-time
      description: дата установки флага частично возвращенного заказа
      example: "2021-06-11T11:27:10.000000Z"
      nullable: true
    created_at:
      type: string
      format: date-time
      description: дата создания заказа
      example: "2021-06-11T11:27:10.000000Z"
    updated_at:
      type: string
      format: date-time
      description: дата обновления заказа
      example: "2021-06-11T11:27:10.000000Z"
  required:
    - id
    - number
    - customer_id
    - customer_email
    - cost
    - price
    - spent_bonus
    - added_bonus
    - promo_code
    - status_at
    - source
    - payment_status
    - payment_status_at
    - payed_at
    - payed_price
    - payment_expires_at
    - payment_method
    - payment_system
    - payment_link
    - payment_external_id
    - payment_data
    - is_problem_at
    - is_expired
    - is_expired_at
    - is_return
    - is_return_at
    - is_partial_return
    - is_partial_return_at
    - created_at
    - updated_at

OrderFillableProperties:
  type: object
  properties:
    responsible_id:
      type: integer
      description: Идентификатор ответственного за заказ
      example: 1
      nullable: true
    status:
      type: integer
      description: статус заказа из OrderStatus
      example: 1
    client_comment:
      type: string
      description: комментарий клиента
      example: "какой-то комментарий"
      nullable: true
    receiver_name:
      type: string
      description: имя получателя
      example: "Иванов Иван Иванович"
      nullable: true
    receiver_phone:
      type: string
      description: телефон получателя
      example: "+79998887766"
      nullable: true
    receiver_email:
      type: string
      description: e-mail получателя
      example: "ivanov@example.ru"
      nullable: true
    is_problem:
      type: boolean
      description: флаг, что заказ проблемный
      example: true
      nullable: true
    problem_comment:
      type: string
      description:  последнее сообщение продавца о проблеме со сборкой
      example: "нет части корзины"
      nullable: true

OrderDeliveryFillableProperties:
  type: object
  properties:
    delivery_service:
      type: integer
      description: служба доставки
      example: 11
    delivery_method:
      type: integer
      description: метод доставки
      example: 1
    delivery_cost:
      type: integer
      description: стоимость доставки (без учета скидки) в копейках
      example: 20000
    delivery_price:
      type: integer
      description: стоимость доставки (с учетом скидки) в копейках
      example: 20000
    delivery_point_id:
      type: integer
      description: ID пункта самовывоза из сервиса логистики
      example: 55
      nullable: true
    delivery_address:
      $ref: './data/address.yaml#/Address'
    delivery_comment:
      type: string
      description: комментарий к доставке
      example: "Остановиться перед воротами"
      nullable: true

OrderFillableRequiredProperties:
  required:
    - responsible_id
    - status
    - client_comment
    - receiver_name
    - receiver_phone
    - receiver_email
    - is_problem
    - problem_comment

OrderDeliveryFillableRequiredProperties:
  required:
    - delivery_service
    - delivery_method
    - delivery_cost
    - delivery_price
    - delivery_point_id
    - delivery_address
    - delivery_comment

OrderIncludes:
  type: object
  properties:
    items:
      type: array
      items:
        $ref: './order_items.yaml#/OrderItem'
    deliveries:
      type: array
      items:
        $ref: './deliveries.yaml#/Delivery'
    files:
      type: array
      items:
        $ref: './order_files.yaml#/OrderFile'

Order:
  allOf:
    - $ref: '#/OrderReadonlyProperties'
    - $ref: '#/OrderFillableProperties'
    - $ref: '#/OrderFillableRequiredProperties'
    - $ref: '#/OrderDeliveryFillableProperties'
    - $ref: '#/OrderDeliveryFillableRequiredProperties'
    - $ref: '#/OrderIncludes'

PatchOrderRequest:
  allOf:
    - $ref: '#/OrderFillableProperties'

PatchOrderDeliveryRequest:
  allOf:
    - $ref: '#/OrderDeliveryFillableProperties'


SearchOrdersRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      type: object
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

OrderResponse:
  type: object
  properties:
    data:
      $ref: '#/Order'
    meta:
      type: object
  required:
    - data

SearchOrdersResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Order'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

OrderStartPaymentRequest:
  type: object
  properties:
    return_url:
      type: string
      description: Ссылка, на которую система оплаты должна вернуть пользователя
  required:
    - return_url

OrderStartPaymentResponse:
  type: object
  properties:
    data:
      type: object
      properties:
        payment_link:
          type: string
          description: Ссылка на оплату
      required:
        - payment_link
  required:
    - data

OrderChangePaymentSystemRequest:
  type: object
  properties:
    payment_system:
      type: integer
      description: Система оплаты из PaymentSystemEnum
  required:
    - payment_system

OrderCheckPaymentResponse:
  type: object
  properties:
    data:
      type: object
      properties:
        payment_status:
          type: integer
          description: статус оплаты из PaymentStatusEnum
          example: 1
      required:
        - payment_status
  required:
    - data

OrderAddCommentRequest:
  type: object
  properties:
    text:
      type: string
      description: Текст комментария
  required:
    - text

OrderPaymentData:
  type: object
  properties:
    qr_code:
      type: object
      nullable: true
      properties:
        path:
          type: string
          description: Путь до файла относительно корня диска домена
          example: "attachments/48/2f/image.png"
        root_path:
          type: string
          description: Путь до файла относительно корня физического диска ensi
          example: "protected/domain/attachments/48/2f/image.png"
        url:
          type: string
          description: Ссылка для скачивания файла
          example: "https://es-public.project.ru/domain/attachments/48/2f/image.png"


