<?php

namespace App\Http\Web\Controllers;

use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Date;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер, который эмулирует работу внешней системы оплаты.
 * Есть страница оплаты, которая редиректит обратно на платформу, при этом делая запрос к хэндлеру OMS.
 */
class LocalPaymentController extends Controller
{
    public function index(Request $request)
    {
        $paymentId = $request->get('paymentId');
        if (!$paymentId) {
            throw new NotFoundHttpException();
        }
        /** @var Order $order */
        $order = Order::query()->where('payment_external_id', $paymentId)->firstOrFail();

        if ($order->payment_expires_at < Date::now() || $order->payment_status == PaymentStatusEnum::CANCELED) {
            throw new ValidateException('Ссылка для оплаты недействительна');
        }

        $done = $request->get('done');

        if (!$done) {
            return view('payment', [
                'doneLink' => route('paymentPage', ['paymentId' => $paymentId, 'done' => 'sync']),
            ]);
        } else {
            $data = $order->payment_data;
            $data['done'] = true;
            $order->payment_data = $data;
            $order->save();

            $client = new Client();
            $client->post($order->payment_data['handlerUrl'], [
                'json' => [
                    'paymentId' => $paymentId,
                    'status' => 'done',
                ],
            ]);

            return redirect($order->payment_data['returnLink']);
        }
    }

    public function successPayed(Request $request)
    {
        return view('success-payed', [
            'orderNumber' => $request->get('orderNumber'),
        ]);
    }

    public function paymentFailed(Request $request)
    {
        return view(
            'payment-failed',
            [
                'orderNumber' => $request->get('orderNumber'),
            ]
        );
    }
}
