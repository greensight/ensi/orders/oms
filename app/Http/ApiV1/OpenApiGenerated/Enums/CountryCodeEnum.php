<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Код страны. Расшифровка значений:
 * * `RU` - Российская Федерация
 */
enum CountryCodeEnum: string
{
    /** Российская Федерация */
    case RU = 'RU';
}
