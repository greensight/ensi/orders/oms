<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\OrderItem\ChangeOrderItemsQtyAction;
use App\Domain\Orders\Actions\OrderItem\DeleteOrderItemsAction;
use App\Domain\Orders\Actions\UpsertOrders\AddOrderItems\AddOrderItemsAction;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\Modules\Orders\Requests\AddOrderItemsRequest;
use App\Http\ApiV1\Modules\Orders\Requests\ChangeOrderItemQtyRequest;
use App\Http\ApiV1\Modules\Orders\Requests\DeleteOrderItemsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OrderItemsResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrdersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OrderItemsController
{
    /**
     * @throws ValidateException
     */
    public function changeQty(
        int $id,
        ChangeOrderItemQtyRequest $request,
        ChangeOrderItemsQtyAction $action
    ): AnonymousResourceCollection {
        return OrderItemsResource::collection($action->execute($id, $request->validated()));
    }

    /**
     * @throws ValidateException
     */
    public function add(
        int $id,
        AddOrderItemsRequest $request,
        AddOrderItemsAction $action
    ): OrdersResource {
        return new OrdersResource($action->execute($id, $request->getOrderItems()));
    }

    public function delete(int $id, DeleteOrderItemsRequest $request, DeleteOrderItemsAction $action): Responsable
    {
        $action->execute($id, $request->getOfferIds());

        return new EmptyResource();
    }
}
