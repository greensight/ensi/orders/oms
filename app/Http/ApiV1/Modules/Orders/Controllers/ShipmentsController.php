<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Shipment\PatchShipmentAction;
use App\Http\ApiV1\Modules\Orders\Queries\ShipmentsQuery;
use App\Http\ApiV1\Modules\Orders\Requests\PatchShipmentRequest;
use App\Http\ApiV1\Modules\Orders\Resources\ShipmentsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ShipmentsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ShipmentsQuery $query): AnonymousResourceCollection
    {
        return ShipmentsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, ShipmentsQuery $query): ShipmentsResource
    {
        return new ShipmentsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchShipmentRequest $request, PatchShipmentAction $action): ShipmentsResource
    {
        return new ShipmentsResource($action->execute($id, $request->validated()));
    }
}
