<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Illuminate\Validation\Rules\Enum;

class PatchOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'responsible_id' => ['nullable', 'integer'],
            'status' => [new Enum(OrderStatusEnum::class)],
            'client_comment' => ['nullable', 'string'],
            'receiver_name' => ['nullable', 'string'],
            'receiver_phone' => ['nullable', 'string', new PhoneRule()],
            'receiver_email' => ['nullable', 'string'],
            'is_problem' => ['boolean'],
            'problem_comment' => ['string'],
            'delivery_address' => ['nullable', 'array'],
        ];
    }
}
