<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteOrderItemsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'offer_ids' => ['required', 'array', 'min:1'],
            'offer_ids.*' => ['integer'],
        ];
    }

    public function getOfferIds(): array
    {
        return $this->get('offer_ids');
    }
}
