<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class ChangeOrderPaymentSystemRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_system' => ['required', 'integer', new Enum(PaymentSystemEnum::class)],
        ];
    }

    public function getPaymentSystem(): PaymentSystemEnum
    {
        return PaymentSystemEnum::from($this->get('payment_system'));
    }
}
