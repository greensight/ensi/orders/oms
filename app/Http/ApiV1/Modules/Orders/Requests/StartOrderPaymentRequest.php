<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StartOrderPaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'return_url' => ['required', 'string'],
        ];
    }

    public function getReturnUrl(): string
    {
        return $this->get('return_url');
    }
}
