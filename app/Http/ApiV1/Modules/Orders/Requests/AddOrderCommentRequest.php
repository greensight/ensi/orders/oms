<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddOrderCommentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'text' => ['required', 'string'],
        ];
    }

    public function getText(): string
    {
        return $this->get('text');
    }
}
