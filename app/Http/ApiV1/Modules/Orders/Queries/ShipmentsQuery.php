<?php

namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Orders\Models\Shipment;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ShipmentsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Shipment::query());

        $this->allowedIncludes([
            'delivery',
            AllowedInclude::callback('order_items', function ($query) {
                return $query->orderBy('created_at')->orderBy('id');
            }, 'orderItems'),
        ]);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }
}
