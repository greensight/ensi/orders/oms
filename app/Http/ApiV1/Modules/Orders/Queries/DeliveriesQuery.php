<?php

namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Orders\Models\Delivery;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DeliveriesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Delivery::query());

        $this->allowedIncludes(['shipments', 'shipments.orderItems']);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
        ]);

        $this->defaultSort('id');
    }
}
