<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class DeleteOrderItemsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'offer_ids' => $this->faker->randomList(fn () => $this->faker->unique()->modelId(), 1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
