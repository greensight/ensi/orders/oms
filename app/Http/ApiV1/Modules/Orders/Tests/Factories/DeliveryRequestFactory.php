<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Domain\Orders\Data\Timeslot;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class DeliveryRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'date' => $this->faker->date(),
            'timeslot' => Timeslot::factory()->make()->toArray(),
            'status' => $this->faker->randomEnum(DeliveryStatusEnum::cases()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
