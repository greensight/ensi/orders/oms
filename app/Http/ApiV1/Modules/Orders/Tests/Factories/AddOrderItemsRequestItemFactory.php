<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class AddOrderItemsRequestItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'offer_id' => $this->faker->unique()->modelId(),
            'qty' => $this->faker->randomFloat(2, 1, 1000),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
