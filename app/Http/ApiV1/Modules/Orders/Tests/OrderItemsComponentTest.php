<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Factories\OfferFactory;
use App\Domain\Orders\Models\Factories\ProductFactory;
use App\Domain\Orders\Models\Factories\StockFactory;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Domain\Orders\Support\CalcPrice;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\AddOrderItemsRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\DeleteOrderItemsRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\OffersClient\Dto\Offer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'OrderItemController');

test('POST /api/v1/orders/orders/{id}:add-order-items 200', function (
    bool $isRealActive = true,
    int $status = 200,
) {
    /** @var ApiV1ComponentTestCase $this */

    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->withEditableFlag()
        ->create([
            'is_changed' => false,
        ]);

    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create();
    /** @var Shipment $shipment */
    $shipment = Shipment::factory()->for($delivery)->create();
    OrderItem::factory()->isInit()->for($shipment)->for($order)->create([
        'qty' => 1,
        'price_per_one' => $order->price - $order->delivery_price,
    ]);

    $httpRequest = AddOrderItemsRequestFactory::new()->make();
    $items = collect($httpRequest['order_items']);

    $stock = StockFactory::new()->make(['store_id' => $shipment->store_id]);
    $offersResponse = OfferFactory::new()->makeResponseSearch($items->map(fn (array $item) => [
        'id' => $item['offer_id'],
        'price' => 1000,
        'stocks' => [$stock],
        'is_real_active' => $isRealActive,
    ])->toArray());

    $this->mockOffersOffersApi()->allows(['searchOffers' => $offersResponse]);

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(Arr::map($offersResponse->getData(), fn (Offer $item) => [
            'id' => $item->getProductId(),
        ])),
    ]);

    postJson("/api/v1/orders/orders/{$order->id}:add-order-items", $httpRequest)
        ->assertStatus($status);

    if ($status === 200) {
        /** @var Offer[] $offers */
        $offers = Arr::keyBy($offersResponse->getData(), 'id');
        $addPrice = 0;
        foreach ($items as $item) {
            $itemPrice = CalcPrice::itemPrice($item['qty'], $offers[$item['offer_id']]->getPrice());

            assertDatabaseHas(OrderItem::class, [
                'order_id' => $order->id,
                'offer_id' => $item['offer_id'],
                'qty' => $item['qty'],
                'old_qty' => null,
                'is_added' => true,
                'price' => $itemPrice,
            ]);
            $addPrice += $itemPrice;
        }

        assertDatabaseHas(Order::class, [
            'id' => $order->id,
            'price' => $order->price + $addPrice,
            'is_changed' => true,
        ]);
    } else {
        foreach ($items as $item) {
            assertDatabaseMissing(OrderItem::class, [
                'order_id' => $order->id,
                'offer_id' => $item['offer_id'],
            ]);
        }

        assertDatabaseHas(Order::class, [
            'id' => $order->id,
            'price' => $order->price,
            'is_changed' => false,
        ]);
    }
})->with([
    'success' => [],
    'is_real_active=false' => [false, 400],
]);

test('POST /api/v1/orders/orders/{id}:add-order-items 404', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->create([
            'status' => OrderStatusEnum::NEW,
            'payment_status' => PaymentStatusEnum::NOT_PAID,
        ]);

    $orderId = $order->id + 2;
    $request = AddOrderItemsRequestFactory::new()->make();

    postJson("/api/v1/orders/orders/{$orderId}:add-order-items", $request)
        ->assertStatus(404);
});

test('POST /api/v1/orders/orders/{id}:delete-order-items 200', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->withEditableFlag()
        ->create([
            'is_changed' => false,
        ]);

    /** @var Collection|OrderItem[] $orderItems */
    $orderItems = OrderItem::factory()->isInit()->count(3)->for($order)->create();
    /** @var OrderItem $orderItemSafe */
    $orderItemSafe = OrderItem::factory()->isInit()->for($order)->create([
        'qty' => 1,
        'price_per_one' => (int)($order->price / 2) - $order->delivery_price, // Оставляем часть от начальной цены заказа
    ]);

    $request = DeleteOrderItemsRequestFactory::new()->make([
        'offer_ids' => $orderItems->pluck('offer_id')->all(),
    ]);

    postJson("/api/v1/orders/orders/{$order->id}:delete-order-items", $request)
        ->assertStatus(200);

    foreach ($orderItems as $orderItem) {
        assertDatabaseHas($orderItem::class, [
            'id' => $orderItem->id,
            'is_deleted' => true,
            'qty' => $orderItem->qty,
            'price' => 0,
        ]);
    }
    assertDatabaseHas($orderItemSafe::class, [
        'id' => $orderItemSafe->id,
        'is_deleted' => false,
        'qty' => $orderItemSafe->qty,
    ]);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'price' => $orderItemSafe->price + $order->delivery_price,
        'is_changed' => true,
    ]);
});

test('POST /api/v1/orders/orders/{id}:delete-order-items - delete last item - 400', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->withEditableFlag()
        ->create([
            'is_changed' => false,
        ]);

    /** @var Collection|OrderItem[] $orderItems */
    $orderItems = OrderItem::factory()->isInit()->count(2)->for($order)->create();

    $request = DeleteOrderItemsRequestFactory::new()->make([
        'offer_ids' => $orderItems->pluck('offer_id')->all(),
    ]);

    postJson("/api/v1/orders/orders/{$order->id}:delete-order-items", $request)
        ->assertStatus(400);

    foreach ($orderItems as $orderItem) {
        assertDatabaseHas($orderItem::class, [
            'id' => $orderItem->id,
            'is_deleted' => false,
            'qty' => $orderItem->qty,
        ]);
    }

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'is_changed' => false,
    ]);
});

test('POST /api/v1/orders/orders/{id}:change-order-items-qty 200', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->withEditableFlag()
        ->create();

    $delivery = Delivery::factory()->for($order)->create();
    $shipment = Shipment::factory()->for($delivery)->create();

    /** @var Collection|OrderItem[] $orderItems */
    $orderItems = OrderItem::factory()->isInit()->count(4)->for($shipment)->for($order)->create();
    /** @var OrderItem $orderItemSkip */
    $orderItemSkip = $orderItems->pop();

    $resultOrderPrice = $orderItemSkip->price;
    $requestData = [];
    $dbData = [
        [
            'id' => $orderItemSkip->id,
            'order_id' => $order->id,
            'qty' => $orderItemSkip->qty,
            'old_qty' => $orderItemSkip->old_qty,
            'price' => $orderItemSkip->price,
        ],
    ];
    foreach ($orderItems as $orderItem) {
        $newQty = $orderItem->qty + 2;
        $requestData[] = ['item_id' => $orderItem->id, 'qty' => $newQty];

        $itemPrice = CalcPrice::itemPrice($newQty, $orderItem->price_per_one);
        $dbData[] = ['id' => $orderItem->id, 'qty' => $newQty, 'old_qty' => $orderItem->qty, 'price' => $itemPrice];
        $resultOrderPrice += $itemPrice;
    }

    /** @var OrderItem $orderItemsAlreadyChange */
    $orderItemsAlreadyChange = OrderItem::factory()->isInit()->for($shipment)->for($order)->create([
        'qty' => 3,
        'old_qty' => 5,
    ]);
    $newQty = $orderItemsAlreadyChange->qty + 3;
    $requestData[] = ['item_id' => $orderItemsAlreadyChange->id, 'qty' => $newQty];
    $itemPrice = CalcPrice::itemPrice($newQty, $orderItemsAlreadyChange->price_per_one);
    $dbData[] = ['id' => $orderItemsAlreadyChange->id, 'qty' => $newQty, 'old_qty' => $orderItemsAlreadyChange->old_qty, 'price' => $itemPrice];
    $resultOrderPrice += $itemPrice;

    postJson("/api/v1/orders/orders/{$order->id}:change-order-items-qty", [
        'order_items' => $requestData,
    ])
        ->assertStatus(200);

    foreach ($dbData as $item) {
        assertDatabaseHas(OrderItem::class, array_merge($item, ['order_id' => $order->id]));
    }

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'price' => $resultOrderPrice + $order->delivery_price,
        'is_changed' => true,
    ]);
});

test('POST /api/v1/orders/orders/{id}:change-order-items-qty 400', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');
    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->create();

    $delivery = Delivery::factory()->for($order)->create();
    $shipment = Shipment::factory()->for($delivery)->create();

    /** @var OrderItem $orderItem */
    $orderItem = OrderItem::factory()->isInit()->for($shipment)->for($order)->create();

    postJson("/api/v1/orders/orders/{$order->id}:change-order-items-qty", [
        'order_items' => [
            [
                'item_id' => $orderItem->id + 1,
                'qty' => $orderItem->qty + 2,
            ],
        ],
    ])
        ->assertStatus(400);

    assertDatabaseHas(OrderItem::class, [
        'id' => $orderItem->id,
        'order_id' => $order->id,
        'qty' => $orderItem->qty,
        'old_qty' => $orderItem->old_qty,
        'price' => $orderItem->price,
    ]);
});

test('POST /api/v1/orders/orders/{id}:change-order-items-qty 404', function () {
    $this->mock(SendOrderEventAction::class)->allows('execute');
    $this->mock(SendDeliveryEventAction::class)->allows('execute');

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->create();

    $delivery = Delivery::factory()->for($order)->create();
    $shipment = Shipment::factory()->for($delivery)->create();

    /** @var OrderItem $orderItem */
    $orderItem = OrderItem::factory()->isInit()->for($shipment)->for($order)->create();

    $orderId = $order->id + 2;

    postJson("/api/v1/orders/orders/{$orderId}:change-order-items-qty", [
        'order_items' => [
            [
                'item_id' => $orderItem->id,
                'qty' => $orderItem->qty + 2,
            ],
        ],
    ])
        ->assertStatus(404);
});
