<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\OrderItem;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Resources\MissingValue;

/** @mixin OrderItem */
class OrderItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'order_id' => $this->order_id,
            'offer_id' => $this->offer_id,
            'shipment_id' => $this->shipment_id,

            'name' => $this->name,
            'qty' => $this->qty,
            'old_qty' => $this->old_qty,
            'price' => $this->price,
            'price_per_one' => $this->price_per_one,
            'cost' => $this->cost,
            'cost_per_one' => $this->cost_per_one,

            'refund_qty' => isset($this->refundItem) ? $this->refundItem->qty : new MissingValue(),

            'product_weight' => $this->product_weight,
            'product_weight_gross' => $this->product_weight_gross,
            'product_width' => $this->product_width,
            'product_height' => $this->product_height,
            'product_length' => $this->product_length,
            'product_barcode' => $this->product_barcode,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'is_added' => $this->is_added,
            'is_deleted' => $this->is_deleted,

        ];
    }
}
