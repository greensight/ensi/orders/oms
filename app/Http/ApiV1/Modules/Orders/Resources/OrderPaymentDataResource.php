<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\OrderPaymentData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin OrderPaymentData */
class OrderPaymentDataResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'qr_code' => $this->mapProtectedFileToResponse($this->qrCode),
        ];
    }
}
