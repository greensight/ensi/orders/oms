<?php

namespace App\Http\ApiV1\Modules\Refunds\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RefundReasonRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->text(255),
            'code' => $this->faker->text(20),
            'description' => $this->faker->nullable()->text(1000),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
