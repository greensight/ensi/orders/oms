<?php

use App\Domain\Common\Models\Setting;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Domain\Refunds\Models\Refund;
use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundCreateRequestFactory;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundPatchRequestFactory;
use App\Http\ApiV1\Modules\Refunds\Tests\Factories\RefundReasonRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/refunds/refund-reasons success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $count = 5;
    RefundReason::factory()->count($count)->create();

    getJson("/api/v1/refunds/refund-reasons")
        ->assertStatus(200)
        ->assertJsonCount($count, 'data');
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/refunds/refund-reasons/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var RefundReason $reason */
    $reason = RefundReason::factory()->create([
        'code' => 'REASON_CODE',
        'name' => 'Reason name',
    ]);

    $name = 'New reason name';

    $reasonData = RefundReasonRequestFactory::new()->only(['name'])->make(['name' => $name]);

    patchJson("/api/v1/refunds/refund-reasons/{$reason->id}", $reasonData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $reason->id)
        ->assertJsonPath('data.name', $name)
        ->assertJsonPath('data.code', $reason->code);

    assertDatabaseHas((new RefundReason())->getTable(), [
        'id' => $reason->id,
        'name' => $name,
        'code' => $reason->code,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refund-reasons success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $reason = RefundReasonRequestFactory::new()->make();

    postJson("/api/v1/refunds/refund-reasons", $reason)
        ->assertStatus(201)
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'description']])
        ->assertJsonPath('data.name', $reason['name'])
        ->assertJsonPath('data.code', $reason['code']);

    assertDatabaseHas((new RefundReason())->getTable(), [
        'name' => $reason['name'],
        'code' => $reason['code'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refunds success', function (
    bool $fullRefund,
    RefundStatusEnum $status,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    /** @var Order $order */
    $order = Order::factory()->create(['is_return' => false, 'is_partial_return' => false]);
    $delivery = Delivery::factory()->for($order)->done()->create();
    $shipment = Shipment::factory()->for($delivery)->create();
    $orderItems = OrderItem::factory()->isInit()->count(2)->for($shipment)->for($order)->create();
    $reasons = RefundReason::factory()->count(2)->create();

    $refund = RefundCreateRequestFactory::new()
        ->withItems($orderItems, $fullRefund)
        ->withReasons($reasons)
        ->make([
            'order_id' => $order->id,
            'status' => $status,
        ]);

    postJson('/api/v1/refunds/refunds', $refund)
        ->assertStatus(201)
        ->assertJsonPath('data.order_id', $refund['order_id'])
        ->assertJsonPath('data.is_partial', !$fullRefund);

    assertDatabaseHas((new Refund())->getTable(), [
        'order_id' => $refund['order_id'],
        'is_partial' => !$fullRefund,
    ]);
})->with([
    'is full approved refund' => [true, RefundStatusEnum::APPROVED],
    'is partial approved refund' => [false, RefundStatusEnum::APPROVED],
    'is full new refund' => [true, RefundStatusEnum::NEW],
    'is partial new refund' => [false, RefundStatusEnum::NEW],
], FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refunds exceeded return time', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    $minutes = Setting::getValue(SettingCodeEnum::REFUND);
    $order = Order::factory()->create([
        'is_return' => false,
        'is_partial_return' => false,
        'created_at' => Date::now()->subMinutes((int) $minutes)->subSecond()->format('Y-m-d H:i:s'),
    ]);
    $delivery = Delivery::factory()->for($order)->done()->create();
    $shipment = Shipment::factory()->for($delivery)->create();
    $orderItems = OrderItem::factory()->count(2)->for($shipment)->for($order)->create();
    $reasons = RefundReason::factory()->count(2)->create();

    $refund = RefundCreateRequestFactory::new()
        ->withItems($orderItems, true)
        ->withReasons($reasons)
        ->make([
            'order_id' => $order->id,
            'status' => RefundStatusEnum::APPROVED,
        ]);

    postJson('/api/v1/refunds/refunds', $refund)
        ->assertStatus(400);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'is_return' => false,
        'is_partial_return' => false,
    ]);

    assertDatabaseMissing((new Refund())->getTable(), [
        'order_id' => $order->id,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refunds:search success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $refunds = Refund::factory()
        ->count(10)
        ->sequence(
            ['status' => RefundStatusEnum::APPROVED],
            ['status' => RefundStatusEnum::NEW],
        )
        ->create();

    postJson("/api/v1/refunds/refunds:search", [
        "filter" => ["status" => RefundStatusEnum::NEW],
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $refunds->last()->id)
        ->assertJsonPath('data.0.status', RefundStatusEnum::NEW->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/refunds/refunds:search filter order_id success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create();
    /** @var Refund $refund */
    $refund = Refund::factory()->create(['order_id' => $order->id]);

    postJson("/api/v1/refunds/refunds:search", ["filter" => [
        'order_id' => [$order->id, $order->id + 1],
    ]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $refund->id);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/refunds/refunds:search filter success", function (
    string $fieldKey,
    $value = null,
    ?string $filterKey = null,
    $filterValue = null,
    ?bool $always = null,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var Refund $refund */
    $refund = Refund::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/refunds/refunds:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $refund->{$fieldKey}),
    ]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $refund->id);
})->with([
    ['id', 1, 'id', [1, 2]],
    ['manager_id', 1, 'manager_id', [1, 2]],
    ['responsible_id', 1, 'responsible_id', [1, 2]],
    ['source', 1, 'source', [1, 2]],
    ['status', 1, 'status', [1, 2]],
    ['created_at', now()->subDay(), 'created_at_gte', now()->subDays(2)],
    ['created_at', now()->subDay(), 'created_at_lte', now()->addDays(2)],
    ['updated_at', now()->subDay(), 'updated_at_gte', now()->subDays(2)],
    ['updated_at', now()->subDay(), 'updated_at_lte', now()->addDays(2)],
], FakerProvider::$optionalDataset);

test("POST /api/v1/refunds/refunds:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Refund::factory()->create();
    postJson("/api/v1/refunds/refunds:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'order_id',
    'source',
    'status',
    'created_at',
    'updated_at',
], FakerProvider::$optionalDataset);

test('GET /api/v1/refunds/refunds/{id}?include=reasons success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $count = 3;
    $reasons = RefundReason::factory()->count($count);

    /** @var Refund $refund */
    $refund = Refund::factory()
        ->hasAttached(
            factory: $reasons,
            relationship: 'reasons'
        )
        ->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=reasons")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['reasons' => [['id', 'code', 'name', 'description']]]])
        ->assertJsonCount($count, 'data.reasons');
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/refunds/refunds/{id}?include=items success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $count = 3;
    $items = OrderItem::factory()->count($count);

    /** @var Refund $refund */
    $refund = Refund::factory()
        ->hasAttached(
            factory: $items,
            pivot: ['qty' => 1],
            relationship: 'items'
        )
        ->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=items")
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => ['items' => [['id', 'order_id', 'offer_id', 'name', 'qty', 'price', 'cost', 'refund_qty']]],
        ])
        ->assertJsonCount($count, 'data.items');
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/refunds/refunds/{id}?include=order success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create();

    /** @var Refund $refund */
    $refund = Refund::factory()->for($order)->create();

    getJson("/api/v1/refunds/refunds/$refund->id?include=order")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['order' => ['id', 'cost', 'price']]])
        ->assertJsonPath('data.order.id', $order->id)
        ->assertJsonPath('data.order.cost', $order->cost)
        ->assertJsonPath('data.order.price', $order->price);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/refunds/refunds/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    /** @var Refund $refund */
    $refund = Refund::factory()->create([
        'status' => RefundStatusEnum::NEW,
        'responsible_id' => 123,
    ]);

    $status = RefundStatusEnum::APPROVED;

    $refundData = RefundPatchRequestFactory::new()->only(['status'])->make(['status' => $status]);

    patchJson("/api/v1/refunds/refunds/{$refund->id}", $refundData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refund->id)
        ->assertJsonPath('data.status', $status->value)
        ->assertJsonPath('data.responsible_id', $refund->responsible_id);

    assertDatabaseHas((new Refund())->getTable(), [
        'id' => $refund->id,
        'status' => $status,
        'responsible_id' => $refund->responsible_id,
    ]);
})->with(FakerProvider::$optionalDataset);
