<?php

namespace App\Http\ApiV1\Modules\Refunds\Controllers;

use App\Domain\Refunds\Actions\Refund\AttachRefundFileAction;
use App\Domain\Refunds\Actions\Refund\CreateRefundAction;
use App\Domain\Refunds\Actions\Refund\DeleteRefundFilesAction;
use App\Domain\Refunds\Actions\Refund\PatchRefundAction;
use App\Domain\Refunds\Actions\RefundReason\CreateRefundReasonAction;
use App\Domain\Refunds\Actions\RefundReason\PatchRefundReasonAction;
use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\Modules\Refunds\Queries\RefundsQuery;
use App\Http\ApiV1\Modules\Refunds\Requests\AttachRefundFileRequest;
use App\Http\ApiV1\Modules\Refunds\Requests\CreateRefundReasonRequest;
use App\Http\ApiV1\Modules\Refunds\Requests\CreateRefundRequest;
use App\Http\ApiV1\Modules\Refunds\Requests\DeleteRefundFilesRequest;
use App\Http\ApiV1\Modules\Refunds\Requests\PatchRefundReasonRequest;
use App\Http\ApiV1\Modules\Refunds\Requests\PatchRefundRequest;
use App\Http\ApiV1\Modules\Refunds\Resources\RefundFilesResource;
use App\Http\ApiV1\Modules\Refunds\Resources\RefundReasonsResource;
use App\Http\ApiV1\Modules\Refunds\Resources\RefundsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class RefundsController
{
    public function create(CreateRefundRequest $request, CreateRefundAction $action): Responsable
    {
        return new RefundsResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, RefundsQuery $query): Responsable
    {
        return RefundsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, RefundsQuery $query): Responsable
    {
        return new RefundsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchRefundRequest $request, PatchRefundAction $action): Responsable
    {
        return new RefundsResource($action->execute($id, $request->validated()));
    }

    public function attach(int $id, AttachRefundFileRequest $request, AttachRefundFileAction $action): Responsable
    {
        return new RefundFilesResource($action->execute($id, $request->file('file')));
    }

    public function deleteFiles(int $id, DeleteRefundFilesRequest $request, DeleteRefundFilesAction $action): Responsable
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }

    public function getRefundReasons(): Responsable
    {
        return RefundReasonsResource::collection(RefundReason::all());
    }

    public function createReason(CreateRefundReasonRequest $request, CreateRefundReasonAction $action): Responsable
    {
        return new RefundReasonsResource($action->execute($request->validated()));
    }

    public function patchReason(int $id, PatchRefundReasonRequest $request, PatchRefundReasonAction $action): Responsable
    {
        return new RefundReasonsResource($action->execute($id, $request->validated()));
    }
}
