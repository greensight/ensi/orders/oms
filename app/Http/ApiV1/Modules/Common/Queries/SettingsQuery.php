<?php

namespace App\Http\ApiV1\Modules\Common\Queries;

use App\Domain\Common\Models\Setting;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SettingsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Setting::query());

        $this->allowedSorts(['id', 'code', 'name', 'value', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            ...StringFilter::make('name')->contain(),
            AllowedFilter::exact('value'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
