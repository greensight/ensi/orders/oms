<?php

use App\Domain\Common\Models\Setting;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Models\Refund;
use App\Http\ApiV1\Modules\Common\Tests\Factories\CustomerPersonalDataRequestFactory;
use App\Http\ApiV1\Modules\Common\Tests\Factories\DeleteCustomerPersonalDataRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 200 can_delete is equal true', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customerId = 1;

    Setting::query()->updateOrCreate(['code' => SettingCodeEnum::REFUND], ['value' => 0]);

    Order::factory()->createMany([
        ['status' => OrderStatusEnum::DONE, 'customer_id' => $customerId],
        ['status' => OrderStatusEnum::CANCELED, 'customer_id' => $customerId],
    ]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', true)
        ->assertJsonPath('data.message', '');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 200 can_delete is equal false with not available order status', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customerId = 1;

    Order::factory()->createMany([
        ['status' => OrderStatusEnum::WAIT_PAY, 'customer_id' => $customerId],
        ['status' => OrderStatusEnum::APPROVED, 'customer_id' => $customerId],
    ]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', false)
        ->assertJsonPath('data.message', trans('customer_delete_messages.incomplete_orders'));
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 200 can_delete is equal false with not done refund time', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customerId = 1;
    $minutesForRefundOrder = (int)Setting::getValue(SettingCodeEnum::REFUND);

    $time = Date::now()->subMinutes($minutesForRefundOrder)->subMinute();

    $order = Order::factory()->createMany([
        ['status' => OrderStatusEnum::DONE, 'customer_id' => $customerId, 'status_at' => $time],
        ['status' => OrderStatusEnum::CANCELED, 'customer_id' => $customerId],
    ]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    $responseMessage = trans('customer_delete_messages.incomplete_refund_time_in_order', ['orderId' => $order[0]->id]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', false)
        ->assertJsonPath('data.message', $responseMessage);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:check-conditions-to-delete 200 can_delete is equal false with not done refund status', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customerId = 1;

    $order = Order::factory()->createOne(['status' => OrderStatusEnum::DONE, 'customer_id' => $customerId]);

    Refund::factory()->createOne(['status' => RefundStatusEnum::NEW, 'order_id' => $order->id]);

    $request = CustomerPersonalDataRequestFactory::new()->make(['customer_id' => $customerId]);

    $responseMessage = trans('customer_delete_messages.incomplete_refunds_in_order', ['orderId' => $order->id]);

    postJson('/api/v1/common/customer-personal-data:check-conditions-to-delete', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.can_delete', false)
        ->assertJsonPath('data.message', $responseMessage);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:delete 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->createOne(['status' => OrderStatusEnum::CANCELED]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $order->customer_id]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(200);

    assertDatabaseHas(Order::class, [
        'customer_id' => $order->customer_id,
        'receiver_name' => null,
        'receiver_phone' => null,
        'receiver_email' => null,
        'delivery_address' => null,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:delete 200 not delete personal data at another customer', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $firstCustomer = 1;
    $secondCustomer = 2;

    /** @var Order $firstCustomerOrder */
    $firstCustomerOrder = Order::factory()
        ->withPersonalData()
        ->createOne([
            'status' => OrderStatusEnum::CANCELED,
            'customer_id' => $firstCustomer,
        ]);

    /** @var Order $secondCustomerOrder */
    $secondCustomerOrder = Order::factory()
        ->withPersonalData()
        ->createOne([
            'status' => OrderStatusEnum::CANCELED,
            'customer_id' => $secondCustomer,
        ]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $firstCustomerOrder->customer_id]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(200);

    assertDatabaseHas(Order::class, [
        'customer_id' => $firstCustomerOrder->customer_id,
        'receiver_name' => null,
        'receiver_phone' => null,
        'receiver_email' => null,
        'delivery_address' => null,
    ]);

    assertDatabaseHas(Order::class, [
        'customer_id' => $secondCustomerOrder->customer_id,
        'receiver_name' => $secondCustomerOrder->receiver_name,
        'receiver_phone' => $secondCustomerOrder->receiver_phone,
        'receiver_email' => $secondCustomerOrder->receiver_email,
        'delivery_address->address_string' => $secondCustomerOrder->delivery_address->address_string,
    ]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/common/customer-personal-data:delete 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()
        ->withPersonalData()
        ->createOne(['status' => OrderStatusEnum::APPROVED]);

    $request = DeleteCustomerPersonalDataRequestFactory::new()->make(['customer_id' => $order->customer_id]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    postJson('/api/v1/common/customer-personal-data:delete', $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);
