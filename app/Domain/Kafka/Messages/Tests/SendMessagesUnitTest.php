<?php

use App\Domain\Kafka\Messages\Send\ModelEvent\DeliveryPayload;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\OrderPayload;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNull;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region DeliveryOrderEventMessage
test("generate DeliveryOrderEventMessage success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->makeOne();
    $payload = new OrderPayload($order);

    $modelEvent = new ModelEventMessage($order, $payload, ModelEventMessage::CREATE, 'orders');
    assertNull($modelEvent->dirty);

    $modelEvent = new ModelEventMessage($order, $payload, ModelEventMessage::UPDATE, 'orders');
    assertIsArray($modelEvent->dirty);
})->with(FakerProvider::$optionalDataset);
// endregion

// region OrderPayload
test("generate OrderPayload success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->withDeliveryAddress()->create()->refresh();

    $payload = new OrderPayload($order);

    assertEquals($payload->jsonSerialize(), $order->attributesToArray());
})->with(FakerProvider::$optionalDataset);
// endregion

// region DeliveryPayload
test("generate DeliveryPayload success", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->create()->refresh();

    $payload = new DeliveryPayload($delivery);

    assertEquals($payload->jsonSerialize(), array_merge($delivery->attributesToArray(), [
        'delivery_method' => $delivery->order->delivery_method,
    ]));
});
// endregion
