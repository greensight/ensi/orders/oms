<?php

namespace App\Domain\Kafka\Messages\Listen\DeliveryOrder;

use Illuminate\Support\Fluent;

/**
 * Class DeliveryOrderPayload
 * @package App\Domain\Kafka\Payloads\Listen
 *
 * @property int $id
 * @property int $delivery_id
 * @property int $status
 */
class DeliveryOrderPayload extends Fluent
{
}
