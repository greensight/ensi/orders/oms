<?php

namespace App\Domain\Kafka\Messages\Listen\DeliveryOrder;

use Illuminate\Support\Fluent;
use RdKafka\Message;

/**
 * Class DeliveryOrderEventMessage
 * @package App\Domain\Kafka\Messages\Listen
 *
 * @property array $dirty
 * @property DeliveryOrderPayload $attributes
 * @property string $event
 */
class DeliveryOrderEventMessage extends Fluent
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct($attributes = [])
    {
        $attributes['attributes'] = new DeliveryOrderPayload($attributes['attributes']);
        parent::__construct($attributes);
    }

    public static function makeFromRdKafka(Message $message): self
    {
        return new self(json_decode($message->payload, true));
    }
}
