<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Models\Setting;
use Illuminate\Support\Arr;

class PatchSeveralSettingsAction
{
    public function execute(array $infoForUpdateWithId): array
    {
        $ids = Arr::pluck($infoForUpdateWithId['settings'], 'id');
        $result = [];
        if ($ids) {
            $settings = Setting::query()->whereIn('id', $ids)->get()->keyBy('id');
            foreach ($infoForUpdateWithId['settings'] as $infoWithId) {
                if (isset($settings[$infoWithId['id']])) {
                    $settings[$infoWithId['id']]->update($infoWithId);
                    $result[] = $settings[$infoWithId['id']];
                }
            }
        }

        return $result;
    }
}
