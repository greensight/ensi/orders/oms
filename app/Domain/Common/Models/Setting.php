<?php

namespace App\Domain\Common\Models;

use App\Domain\Common\Models\Tests\Factories\SettingFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id ID параметра
 * @property string $name название параметра
 * @property SettingCodeEnum $code уникальный код-название параметра
 * @property string $value значение параметра
 *
 * @property CarbonInterface|null $created_at дата создание
 * @property CarbonInterface|null $updated_at дата обновления
 */
class Setting extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'name',
        'value',
    ];

    protected $casts = [
        'code' => SettingCodeEnum::class,
    ];

    public static function factory(): SettingFactory
    {
        return SettingFactory::new();
    }

    public static function getValue(SettingCodeEnum $code): string
    {
        /** @var static|null $setting */
        $setting = static::query()->where("code", $code)->first();
        if ($setting) {
            return $setting->value;
        }

        return match ($code) {
            SettingCodeEnum::PAYMENT => config('common.payment_duration'),
            SettingCodeEnum::PROCESSING => config('common.order_duration'),
            SettingCodeEnum::REFUND => config('common.time_for_refund'),
        };
    }
}
