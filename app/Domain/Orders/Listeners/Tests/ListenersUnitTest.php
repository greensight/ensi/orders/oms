<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Listeners\UpdateDeliveryStatusFromShipmentListener;
use App\Domain\Orders\Listeners\UpdateOrderStatusFromDeliveryListener;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("UpdateDeliveryStatusFromShipmentListener success", function (ShipmentStatusEnum $shipment1Status, ShipmentStatusEnum $shipment2NewStatus, DeliveryStatusEnum $deliveryCheckStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->forApprovedOrder()->create(['status' => DeliveryStatusEnum::NEW]);
    Shipment::factory()->for($delivery)->create(['status' => $shipment1Status]);
    /** @var Shipment $shipment2 */
    $shipment2 = Shipment::factory()->for($delivery)->create(['status' => $shipment2NewStatus]);

    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');
    Event::fake();

    resolve(UpdateDeliveryStatusFromShipmentListener::class)->handle(new ShipmentStatusUpdated($shipment2));

    if ($deliveryCheckStatus != DeliveryStatusEnum::NEW) {
        Event::assertDispatched(DeliveryStatusUpdated::class);
    } else {
        Event::assertNotDispatched(DeliveryStatusUpdated::class);
    }

    assertDatabaseHas(Delivery::class, [
        'id' => $delivery->id,
        'status' => $deliveryCheckStatus,
    ]);
})->with([
    'assembled without changed' => [ShipmentStatusEnum::NEW, ShipmentStatusEnum::ASSEMBLED, DeliveryStatusEnum::NEW],
    'assembled changed' => [ShipmentStatusEnum::ASSEMBLED, ShipmentStatusEnum::ASSEMBLED, DeliveryStatusEnum::ASSEMBLED],
    'cancelled without changed' => [ShipmentStatusEnum::NEW, ShipmentStatusEnum::CANCELED, DeliveryStatusEnum::NEW],
    'cancelled changed' => [ShipmentStatusEnum::CANCELED, ShipmentStatusEnum::CANCELED, DeliveryStatusEnum::CANCELED],
]);

test("UpdateOrderStatusFromDeliveryListener success", function (
    DeliveryStatusEnum $delivery1Status,
    DeliveryStatusEnum $delivery2NewStatus,
    OrderStatusEnum $orderCheckStatus,
    ?bool $always
) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::APPROVED]);
    Delivery::factory()->for($order)->create(['status' => $delivery1Status]);
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create(['status' => $delivery2NewStatus]);

    Event::fake();
    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    resolve(UpdateOrderStatusFromDeliveryListener::class)->handle(new DeliveryStatusUpdated($delivery));

    if ($orderCheckStatus != OrderStatusEnum::APPROVED) {
        Event::assertDispatched(OrderStatusUpdated::class);
    } else {
        Event::assertNotDispatched(OrderStatusUpdated::class);
    }

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => $orderCheckStatus,
    ]);
})->with([
    'without changed' => [DeliveryStatusEnum::ASSEMBLED, DeliveryStatusEnum::DONE, OrderStatusEnum::APPROVED],
    'changed' => [DeliveryStatusEnum::DONE, DeliveryStatusEnum::DONE, OrderStatusEnum::DONE],
], FakerProvider::$optionalDataset);
