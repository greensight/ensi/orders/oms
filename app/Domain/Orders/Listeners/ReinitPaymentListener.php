<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Actions\Payment\PaymentSystem\ReinitPaymentAction;
use App\Domain\Orders\Events\OrderPaymentUpdated;

class ReinitPaymentListener
{
    public function __construct(protected ReinitPaymentAction $reinitPaymentAction)
    {
    }

    public function handle(OrderPaymentUpdated $event): void
    {
        $this->reinitPaymentAction->execute($event->order);
    }
}
