<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CancelPaymentAction;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\OrderStatusUpdated;

class CancelPaymentWhenOrderCanceledListener
{
    public function __construct(
        protected readonly CancelPaymentAction $cancelPaymentAction
    ) {
    }

    public function handle(OrderStatusUpdated $event): void
    {
        $order = $event->order;

        if (in_array($order->status, OrderStatus::cancelled())) {
            $this->cancelPaymentAction->execute($order);
        }
    }
}
