<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Data\RefundStatus;
use App\Domain\Refunds\Events\RefundStatusSaving;
use App\Domain\Refunds\Models\Refund;

/**
 * Пересчет информации о полном и частичном возврате заказа
 */
class CalcOrderIsRefundListener
{
    public function __construct(protected SaveOrderAction $saveOrderAction)
    {
    }

    public function handle(RefundStatusSaving $event): void
    {
        if (!in_array($event->refund->status, RefundStatus::approved())) {
            return;
        }

        /** @var Order $order */
        $order = Order::query()->findOrFail($event->refund->order_id);

        if ($order->is_return) {
            return;
        }

        # Все подтвержденные возвраты по заказу $orderId
        $refunds = Refund::query()
            ->with('items')
            ->where('order_id', $event->refund->order_id)
            ->whereIn('status', RefundStatus::approved())
            ->get();

        if ($refunds->isEmpty()) {
            return;
        }

        $requests = [];

        /** @var Refund $refund */
        foreach ($refunds as $refund) {
            foreach ($refund->items as $item) {
                $requests[$item->id] ??= 0;
                $requests[$item->id] += $item->refundItem->qty;
            }
        }

        # Все товары полностью возвращены?
        foreach ($order->getRealItems() as $item) {
            if (!isset($requests[$item->id]) || $requests[$item->id] < $item->getRealQty()) {
                $order->is_partial_return = true;
                $this->saveOrderAction->execute($order);

                return;
            }
        }

        $order->is_return = true;
        $this->saveOrderAction->execute($order);
    }
}
