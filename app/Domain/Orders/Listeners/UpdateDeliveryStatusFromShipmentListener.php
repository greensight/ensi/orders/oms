<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Data\ShipmentStatus;
use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;

class UpdateDeliveryStatusFromShipmentListener
{
    public function handle(ShipmentStatusUpdated $event): void
    {
        $shipment = $event->shipment;
        $shipment->loadMissing('delivery');

        // Если отменились все отгрузки - отправление тоже отменяем
        if (!$shipment->delivery->hasAvailableShipments()) {
            $this->updateDeliveryStatus($shipment->delivery, DeliveryStatusEnum::CANCELED);

            return;
        }

        if (!in_array($shipment->status, ShipmentStatus::assembled())) {
            return;
        }

        if (in_array($shipment->delivery->status, DeliveryStatus::assembled())) {
            return;
        }

        $shipment->delivery->loadMissing('shipments');
        foreach ($shipment->delivery->shipments as $deliveryShipment) {
            if ($deliveryShipment->id == $shipment->id) { // Текущую отгрузку не смотрим вообще
                continue;
            }

            if (!in_array($deliveryShipment->status, ShipmentStatus::assembled())) {
                return; // Если хоть у одной отгрузки статус - не собран, то ничего не делаем
            }
        }

        // Если мы тут, значит все отгрузки доставки имеют статус собрано и доставке тоже нужно проставить такой статус
        $this->updateDeliveryStatus($shipment->delivery, DeliveryStatusEnum::ASSEMBLED);
    }

    private function updateDeliveryStatus(Delivery $delivery, DeliveryStatusEnum $status): void
    {
        $delivery->status = $status;
        $delivery->save();
        DeliveryStatusUpdated::dispatch($delivery);
    }
}
