<?php

namespace App\Domain\Orders\Actions\UpsertOrders\Stages;

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;

class MakeShipmentAction
{
    public function execute(
        int $storeId,
        Delivery $delivery,
    ): Shipment {
        $shipment = new Shipment();
        $shipment->setRelation('delivery', $delivery);
        $shipment->delivery_id = $delivery->id;
        $shipment->status = ShipmentStatusEnum::NEW;
        $shipment->store_id = $storeId;
        $shipment->number = Shipment::makeNumber($delivery->number, $delivery->shipments->count());
        $shipment->save();

        $delivery->shipments->push($shipment);

        return $shipment;
    }
}
