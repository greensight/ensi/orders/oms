<?php

namespace App\Domain\Orders\Actions\UpsertOrders\Stages;

use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use App\Domain\Orders\Actions\UpsertOrders\Data\ProductInfoData;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;

class MakeOrderItemAction
{
    public function execute(
        OrderItemData $itemData,
        ProductInfoData $productData,
        Shipment $shipment,
        Order $order
    ): OrderItem {
        $orderItem = new OrderItem();
        $orderItem->setRelation('shipment', $shipment);
        $orderItem->setRelation('order', $order);
        $orderItem->shipment_id = $shipment->id;
        $orderItem->order_id = $order->id;

        $orderItem->offer_id = $itemData->offerId;
        $orderItem->qty = $itemData->qty;
        $orderItem->cost_per_one = $itemData->costPerOne;
        $orderItem->price_per_one = $itemData->pricePerOne;

        $orderItem->name = $productData->product->getName();
        $orderItem->product_weight = (int)$productData->product->getWeight();
        $orderItem->product_width = (int)$productData->product->getWidth();
        $orderItem->product_weight_gross = $productData->product->getWeightGross();
        $orderItem->product_barcode = $productData->product->getBarcode();
        $orderItem->product_height = (int)$productData->product->getHeight();
        $orderItem->product_length = (int)$productData->product->getLength();

        return $orderItem;
    }
}
