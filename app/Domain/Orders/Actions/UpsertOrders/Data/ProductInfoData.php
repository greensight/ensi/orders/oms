<?php

namespace App\Domain\Orders\Actions\UpsertOrders\Data;

use Ensi\OffersClient\Dto\Offer;
use Ensi\PimClient\Dto\Product;

class ProductInfoData
{
    public Product $product;
    public Offer $offer;
}
