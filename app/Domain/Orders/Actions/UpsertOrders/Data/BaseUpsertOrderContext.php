<?php

namespace App\Domain\Orders\Actions\UpsertOrders\Data;

use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;

abstract class BaseUpsertOrderContext
{
    public LoggerInterface $logger;

    /** @var ProductInfoData[] */
    protected array $products = [];

    public function getProductInfo(int $offerId): ProductInfoData
    {
        return $this->products[$offerId];
    }

    public function setProductInfo(int $offerId, ProductInfoData $data): void
    {
        $this->products[$offerId] = $data;
    }

    /**
     * @return Collection<OrderItemData>
     */
    abstract public function items(): Collection;
}
