<?php

namespace App\Domain\Orders\Actions\UpsertOrders\AddOrderItems\Data;

use App\Domain\Orders\Actions\UpsertOrders\Data\BaseUpsertOrderContext;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Illuminate\Support\Collection;

class AddOrderItemsContext extends BaseUpsertOrderContext
{
    public Order $order;
    public array $storeToShipment = [];

    public function __construct(
        protected array $items
    ) {
        $this->logger = logger()->channel('add_order_items');
    }

    /**
     * @inheritDoc
     */
    public function items(): Collection
    {
        return collect($this->items);
    }

    public function setOrder($order): void
    {
        $this->order = $order;

        foreach ($this->order->deliveries as $delivery) {
            foreach ($delivery->shipments as $shipment) {
                $this->storeToShipment[$shipment->store_id] = $shipment;
            }
        }
    }

    public function getNewDelivery(): ?Delivery
    {
        foreach ($this->order->deliveries as $delivery) {
            if ($delivery->status === DeliveryStatusEnum::NEW) {
                return $delivery;
            }
        }

        return null;
    }
}
