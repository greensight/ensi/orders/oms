<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data;

use App\Domain\Orders\Data\Timeslot;
use Carbon\CarbonInterface;

class DeliveryData
{
    public CarbonInterface $date;
    public ?Timeslot $timeslot = null;
    public int $cost;
    /** @var ShipmentData[] */
    public array $shipments;
}
