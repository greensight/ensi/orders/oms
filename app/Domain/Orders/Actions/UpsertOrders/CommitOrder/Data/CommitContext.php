<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data;

use App\Domain\Orders\Actions\UpsertOrders\Data\BaseUpsertOrderContext;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use Ensi\OffersClient\Dto\ReserveStocksRequestItems;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class CommitContext extends BaseUpsertOrderContext
{
    protected Order $order;

    /** @var Collection<ReserveStocksRequestItems> */
    protected Collection $reserveItems;

    public function __construct(public CommitData $data)
    {
        $this->reserveItems = collect();
        $this->logger = Log::channel('checkout');
    }

    public function putOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function getReserveItems(): Collection
    {
        return $this->reserveItems;
    }

    public function addReserveItem(OrderItem $orderItem, int $storeId): void
    {
        $reserveItem = (new ReserveStocksRequestItems())
            ->setOfferId($orderItem->offer_id)
            ->setQty($orderItem->getRealQty())
            ->setStoreId($storeId);
        $this->reserveItems->push($reserveItem);
    }

    public function items(): Collection
    {
        return $this->data->items();
    }
}
