<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class AttachOrderFileAction
{
    public function __construct(private EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $orderId, UploadedFile $file): OrderFile
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);

        $hash = Str::random(20);
        $fileName = "order_file_{$order->id}_{$hash}.{$file->getClientOriginalExtension()}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->protectedDiskName());

        $path = $disk->putFileAs("order_files/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new RuntimeException("Unable to save file $fileName to directory order_files/{$hashedSubDirs}");
        }

        $orderFile = new OrderFile();
        $orderFile->order_id = $order->id;
        $orderFile->path = $path;
        $orderFile->original_name = $file->getClientOriginalName();
        $orderFile->save();

        return $orderFile;
    }
}
