<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;

class LoadForEditOrderAction
{
    public function execute(int $orderId): Order
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);

        if (!$order->isEditable()) {
            throw new ValidateException('Данный заказ нельзя редактировать');
        }

        return $order;
    }
}
