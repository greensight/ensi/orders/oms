<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Models\OrderComment;

class AddOrderCommentAction
{
    public function execute(int $orderId, string $text): void
    {
        $comment = new OrderComment();
        $comment->order_id = $orderId;
        $comment->text = $text;

        $comment->save();
    }
}
