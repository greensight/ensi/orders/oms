<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CommitPaymentAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;

class CommitOldPaymentsAction
{
    protected LoggerInterface $logger;

    public function __construct(protected CommitPaymentAction $commitPaymentAction)
    {
        $this->logger = logger()->channel('commit_old_payments');
    }

    public function execute()
    {
        /** @var Collection|Order[] $orders */
        $orders = Order::query()
            ->where('payment_status', PaymentStatusEnum::HOLD)
            ->get();
        $threeDaysAgo = now()->subDays(3);
        foreach ($orders as $order) {
            try {
                if ($threeDaysAgo->greaterThan($order->created_at)) {
                    $this->commitPaymentAction->execute($order);
                }
            } catch (Exception $e) {
                $this->logger->error("Error occurred during attempt to commit old payment. Order ID {$order->id}", ['message' => $e->getMessage()]);
            }
        }
    }
}
