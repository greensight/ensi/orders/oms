<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;

class SetStatusPaymentAction
{
    public function __construct(protected SaveOrderAction $saveOrderAction)
    {
    }

    public function execute(Order $order, PaymentStatusEnum $status): void
    {
        $order->payment_status = $status;

        switch ($status) {
            case PaymentStatusEnum::TIMEOUT:
            case PaymentStatusEnum::CANCELED:
            case PaymentStatusEnum::RETURNED:
                $order->status = OrderStatusEnum::CANCELED;

                break;
            case PaymentStatusEnum::HOLD:
                $order->status = OrderStatusEnum::NEW;

                break;
        }

        $twoFactorSystems = [PaymentSystemEnum::SBER, PaymentSystemEnum::TINKOFF, PaymentSystemEnum::TEST];
        if ($status === PaymentStatusEnum::PAID && !in_array($order->payment_system, $twoFactorSystems)) {
            $order->status = OrderStatusEnum::NEW;
        }

        if ($order->isDirty()) {
            $this->saveOrderAction->execute($order);
        }
    }
}
