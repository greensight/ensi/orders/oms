<?php

namespace App\Domain\Orders\Actions\Payment\PaymentSystem;

use App\Domain\Orders\Actions\Payment\MakePaymentDataAction;
use App\Domain\Orders\Actions\Payment\SaveFromPaymentDataAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;

class ReinitPaymentAction
{
    public function __construct(
        protected MakePaymentDataAction $makePaymentDataAction,
        protected SaveFromPaymentDataAction $savePaymentAction,
    ) {
    }

    public function execute(Order $order): void
    {
        if ($order->payment_method == PaymentMethodEnum::OFFLINE) {
            return;
        }

        $paymentData = $this->makePaymentDataAction->execute($order, true);
        $order->paymentSystem()->reinit($paymentData);
        $this->savePaymentAction->execute($order, $paymentData);
    }
}
