<?php

namespace App\Domain\Orders\Actions\Payment\PaymentSystem;

use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Actions\Payment\MakePaymentDataAction;
use App\Domain\Orders\Actions\Payment\SaveFromPaymentDataAction;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Illuminate\Support\Facades\Date;

class StartPaymentAction
{
    public function __construct(
        protected MakePaymentDataAction $makePaymentDataAction,
        protected SaveFromPaymentDataAction $savePaymentAction,
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    public function execute(Order $order, string $returnUrl, ?string $failUrl = null): string
    {
        if ($order->payment_status != PaymentStatusEnum::NOT_PAID) {
            throw new ValidateException('Статус оплаты не позволяет начать оплату');
        }
        if ($order->payment_method == PaymentMethodEnum::OFFLINE) {
            throw new ValidateException('Способ оплаты не позволяет начать оплату');
        }

        if (!$order->payment_link) {
            $minutes = (int)Setting::getValue(SettingCodeEnum::PAYMENT);
            if ($minutes) {
                $order->payment_expires_at = Date::now()->addMinutes($minutes);
                $this->saveOrderAction->execute($order);
            }

            $paymentData = $this->makePaymentDataAction->execute($order, true);
            $order->paymentSystem()->createExternalPayment($paymentData, $returnUrl, $failUrl);
            $this->savePaymentAction->execute($order, $paymentData);
        }

        return $order->payment_link;
    }
}
