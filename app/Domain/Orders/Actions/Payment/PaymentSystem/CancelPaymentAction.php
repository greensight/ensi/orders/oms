<?php

namespace App\Domain\Orders\Actions\Payment\PaymentSystem;

use App\Domain\Orders\Actions\Payment\MakePaymentDataAction;
use App\Domain\Orders\Actions\Payment\SaveFromPaymentDataAction;
use App\Domain\Orders\Actions\Payment\SetStatusPaymentAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class CancelPaymentAction
{
    public function __construct(
        protected MakePaymentDataAction $makePaymentDataAction,
        protected SaveFromPaymentDataAction $savePaymentAction,
        protected SetStatusPaymentAction $setStatusPaymentAction
    ) {
    }

    public function execute(Order $order): void
    {
        $this->setStatusPaymentAction->execute(
            $order,
            $this->preparePaymentStatus($order)
        );
    }

    protected function preparePaymentStatus(Order $order): PaymentStatusEnum
    {
        if ($order->payment_method == PaymentMethodEnum::OFFLINE) {
            return PaymentStatusEnum::CANCELED;
        }

        $paymentData = $this->makePaymentDataAction->execute($order);
        $order->paymentSystem()->revert($paymentData);

        return $paymentData->status;
    }
}
