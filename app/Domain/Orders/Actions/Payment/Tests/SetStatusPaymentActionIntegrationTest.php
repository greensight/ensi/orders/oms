<?php

use App\Domain\Orders\Actions\Payment\SetStatusPaymentAction;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'payment');

test("change order status to new after paid payment", function (PaymentSystemEnum $paymentSystem) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => PaymentStatusEnum::NOT_PAID,
        'payment_system' => $paymentSystem,
    ]);

    Event::fake();

    resolve(SetStatusPaymentAction::class)->execute($order, PaymentStatusEnum::PAID);

    Event::assertDispatched(OrderStatusUpdated::class);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::NEW,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);
})->with(
    [PaymentSystemEnum::SBP, PaymentSystemEnum::SBER_SBP],
);

test("not change order status to new after paid payment", function (PaymentSystemEnum $paymentSystem) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => PaymentStatusEnum::NOT_PAID,
        'payment_system' => $paymentSystem,
    ]);

    Event::fake();

    resolve(SetStatusPaymentAction::class)->execute($order, PaymentStatusEnum::PAID);

    Event::assertNotDispatched(OrderStatusUpdated::class);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);
})->with(
    [PaymentSystemEnum::SBER],
);

test("change order status to cancel after cancel payment", function (PaymentStatusEnum $paymentStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => $paymentStatus,
    ]);

    Event::fake();

    resolve(SetStatusPaymentAction::class)->execute($order, $paymentStatus);

    Event::assertDispatched(OrderStatusUpdated::class);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::CANCELED,
        'payment_status' => $paymentStatus,
    ]);
})->with(
    [PaymentStatusEnum::CANCELED, PaymentStatusEnum::TIMEOUT, PaymentStatusEnum::RETURNED],
);

test("change order status to new after hold payment", function (PaymentStatusEnum $paymentStatus) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => $paymentStatus,
    ]);

    Event::fake();

    resolve(SetStatusPaymentAction::class)->execute($order, $paymentStatus);

    Event::assertDispatched(OrderStatusUpdated::class);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::NEW,
        'payment_status' => $paymentStatus,
    ]);
})->with(
    [PaymentStatusEnum::HOLD],
);
