<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Models\Shipment;

class CalcShipmentAction
{
    public function __construct(
        protected CalcShipmentSizesAction $calcShipmentSizesAction,
        protected CalcShipmentCostAction $calcShipmentCostAction,
    ) {
    }

    public function execute(Shipment $shipment): void
    {
        $this->calcShipmentCostAction->execute($shipment);
        $this->calcShipmentSizesAction->execute($shipment);
    }
}
