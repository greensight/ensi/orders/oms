<?php

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Actions\Order\PatchOrderAction;
use App\Domain\Orders\Actions\Payment\PaymentSystem\CommitPaymentAction;
use App\Domain\Orders\Actions\Payment\PaymentSystem\RevertPaymentAction;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\Shipment;
use App\Domain\PaymentSystems\Data\PaymentSystem;
use App\Domain\PaymentSystems\Systems\Sber\Tests\DepositFactory;
use App\Domain\PaymentSystems\Systems\Sber\Tests\RefundFactory;
use App\Domain\PaymentSystems\Systems\SberSbp\Tests\CancelSberSbpOrderFactory;
use App\Domain\PaymentSystems\Systems\SberSbp\Tests\RevokeSberSbpOrderFactory;
use App\Domain\PaymentSystems\Systems\Tinkoff\Tests\CancelTinkoffResponseFactory;
use App\Domain\PaymentSystems\Systems\Tinkoff\Tests\ConfirmTinkoffResponseFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Event;

use function Pest\Laravel\assertDatabaseHas;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'PatchOrderAction');

test("PatchOrderAction success", function (array $fields, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::NEW]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    Event::fake();

    resolve(PatchOrderAction::class)->execute($order->id, $fields);

    if (array_key_exists('status', $fields)) {
        Event::assertDispatched(OrderStatusUpdated::class);
    } else {
        Event::assertNotDispatched(OrderStatusUpdated::class);
    }

    assertDatabaseHas(Order::class, array_merge([
        'id' => $order->id,
    ], $fields));
})->with([
    'with status' => [['status' => OrderStatusEnum::APPROVED]],
    'without status' => [['responsible_id' => 1]],
], FakerProvider::$optionalDataset);

test("PatchOrderAction cancel success", function (
    DeliveryStatusEnum $deliveryStatus,
    OrderStatusEnum $orderStatus,
    ?OrderStatusEnum $checkOrderStatus,
    DeliveryStatusEnum $checkDeliveryStatus,
    ShipmentStatusEnum $checkShipmentStatus,
) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create(['status' => $orderStatus]);
    Delivery::factory()->for($order)->create(['status' => DeliveryStatusEnum::ASSEMBLED]);
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create(['status' => $deliveryStatus]);
    Shipment::factory()->for($delivery)->create(['status' => ShipmentStatusEnum::ASSEMBLED]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    $this->mock(CommitPaymentAction::class)->shouldReceive('execute');
    $this->mock(RevertPaymentAction::class)->shouldReceive('execute');

    if ($order->payment_system == PaymentSystemEnum::SBER) {
        $this->mockSberApi()->allows([
            'refundOrder' => RefundFactory::new()->make(),
            'reverseOrder' => RefundFactory::new()->make(),
        ]);
    }

    if ($order->payment_system == PaymentSystemEnum::SBER_SBP) {
        $this->mockSberSbpApi()->allows([
            'revoke' => RevokeSberSbpOrderFactory::new()->makeResponse(),
            'cancel' => CancelSberSbpOrderFactory::new()->makeResponse(),
        ]);
    }

    if (in_array($order->payment_system, [PaymentSystemEnum::TINKOFF, PaymentSystemEnum::TINKOFF_SBP])) {
        $this->mockTinkoffApi()->allows([
            'cancel' => CancelTinkoffResponseFactory::new()->makeResponse(),
        ]);
    }

    try {
        resolve(PatchOrderAction::class)->execute($order->id, ['status' => OrderStatusEnum::CANCELED]);
    } catch (Exception $e) {
    }

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => $checkOrderStatus ?: $orderStatus,
    ]);
    assertDatabaseHas(Delivery::class, [
        'order_id' => $order->id,
        'status' => $checkDeliveryStatus,
    ]);
    assertDatabaseHas(Shipment::class, [
        'delivery_id' => $delivery->id,
        'status' => $checkShipmentStatus,
    ]);
})->with([
    'can change' => [DeliveryStatusEnum::ASSEMBLED, OrderStatusEnum::APPROVED, OrderStatusEnum::CANCELED, DeliveryStatusEnum::CANCELED, ShipmentStatusEnum::CANCELED],
    'can\'t change' => [DeliveryStatusEnum::DONE, OrderStatusEnum::DONE, null, DeliveryStatusEnum::ASSEMBLED, ShipmentStatusEnum::ASSEMBLED],
]);

test("PatchOrderAction payment status cancel for payment systems success", function (
    PaymentStatusEnum $paymentStatus,
    PaymentSystemEnum $paymentSystem,
) {
    /** @var IntegrationTestCase $this */
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::WAIT_PAY,
        'payment_status' => $paymentStatus,
        'payment_system' => $paymentSystem,
    ]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    switch ($paymentSystem) {
        case PaymentSystemEnum::TINKOFF_SBP:
        case PaymentSystemEnum::TINKOFF:
            $this->mockTinkoffApi()
                ->shouldReceive('cancel')
                ->times(1)
                ->andReturn(CancelTinkoffResponseFactory::new()->makeResponse());

            break;
        case PaymentSystemEnum::SBER:
            $this->mockSberApi()->allows([
                'refundOrder' => RefundFactory::new()->make(),
                'reverseOrder' => RefundFactory::new()->make(),
            ]);

            break;
        case PaymentSystemEnum::SBER_SBP:
            $this->mockSberSbpApi()->allows([
                'revoke' => RevokeSberSbpOrderFactory::new()->makeResponse(),
                'cancel' => CancelSberSbpOrderFactory::new()->makeResponse(),
            ]);

            break;
    }

    try {
        resolve(PatchOrderAction::class)->execute($order->id, ['status' => OrderStatusEnum::CANCELED]);
    } catch (Exception) {
    }

    $canReturned = [PaymentSystemEnum::SBER, PaymentSystemEnum::SBER_SBP, PaymentSystemEnum::TINKOFF, PaymentSystemEnum::TINKOFF_SBP];
    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::CANCELED,
        'payment_status' => $paymentStatus == PaymentStatusEnum::PAID && in_array($paymentSystem, $canReturned) ?
            PaymentStatusEnum::RETURNED :
            PaymentStatusEnum::CANCELED,
        'payment_system' => $paymentSystem,
    ]);
})->with(
    [PaymentStatusEnum::PAID, PaymentStatusEnum::NOT_PAID, PaymentStatusEnum::HOLD, PaymentStatusEnum::TIMEOUT],
    PaymentSystemEnum::cases(),
);


test("PatchOrderAction - change order to DONE after HOLD - success", function (PaymentSystemEnum $paymentSystem) {
    /** @var IntegrationTestCase $this */

    // переход в PAID происходит в checkPayment
    $currentPaymentStatus = PaymentSystem::isSbp($paymentSystem) ? PaymentStatusEnum::PAID : PaymentStatusEnum::HOLD;
    /** @var Order $order */
    $order = Order::factory()->withPaymentExternalId()->create([
        'status' => OrderStatusEnum::APPROVED,
        'payment_status' => $currentPaymentStatus,
        'payment_system' => $paymentSystem,
    ]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');
    $this->mock(SendDeliveryEventAction::class)->shouldReceive('execute');

    if (!PaymentSystem::isSbp($paymentSystem)) {
        match ($paymentSystem) {
            PaymentSystemEnum::TINKOFF => $this->mockTinkoffApi()->allows([
                'confirm' => ConfirmTinkoffResponseFactory::new()->makeResponse(),
            ]),
            PaymentSystemEnum::SBER => $this->mockSberApi()->allows([
                'deposit' => DepositFactory::new()->make(),
            ]),
            PaymentSystemEnum::TEST => null,
            default => throw new Exception('Not implemented math for variant'),
        };
    }

    resolve(PatchOrderAction::class)->execute($order->id, ['status' => OrderStatusEnum::DONE]);

    assertDatabaseHas(Order::class, [
        'id' => $order->id,
        'status' => OrderStatusEnum::DONE,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);
})->with(
    array_filter(
        PaymentSystemEnum::cases(),
        fn (PaymentSystemEnum $paymentSystem) => !in_array($paymentSystem->value, [PaymentSystemEnum::CASH->value])
    )
);
