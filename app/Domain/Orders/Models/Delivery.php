<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\Timeslot;
use App\Domain\Orders\Models\Tests\Factories\DeliveryFactory;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Delivery
 * @package App\Models\Delivery
 *
 * @property int $id id отправления
 * @property int $order_id id заказа
 * @property string $number - номер отправления
 * @property int $cost - стоимость отправления, полученная от службы доставки (не влияет на общую стоимость доставки по заказу!) (коп.)
 *
 * @property DeliveryStatusEnum|null $status статус
 * @property CarbonInterface $status_at
 *
 * @property CarbonInterface $date
 * @property Timeslot|null $timeslot
 *
 * @property int $width - ширина (рассчитывается автоматически)
 * @property int $height - высота (рассчитывается автоматически)
 * @property int $length - длина (рассчитывается автоматически)
 * @property int $weight - вес (рассчитывается автоматически)
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Order $order - заказ
 * @property-read Collection|Shipment[] $shipments - отгрузки
 */
class Delivery extends Model implements Auditable
{
    use SupportsAudit;

    protected $fillable = [
        'date',
        'timeslot',
        'status',
    ];

    protected $table = 'deliveries';

    protected $casts = [
        'date' => 'date:Y-m-d',
        'status_at' => 'datetime',
        'status' => DeliveryStatusEnum::class,
    ];

    /**
     * @param string $orderNumber - номер заказа
     * @param int $i - порядковый номер отправления в заказе
     * @return string
     */
    public static function makeNumber(string $orderNumber, int $i): string
    {
        return $orderNumber . '-' . $i;
    }

    public static function factory(): DeliveryFactory
    {
        return DeliveryFactory::new();
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function shipments(): HasMany
    {
        return $this->hasMany(Shipment::class);
    }

    public function hasAvailableShipments(): bool
    {
        $this->loadMissing('shipments');

        foreach ($this->shipments as $shipment) {
            if (!$shipment->isCanceled()) {
                return true;
            }
        }

        return false;
    }

    protected function getTimeslotAttribute(): ?Timeslot
    {
        return !is_null($this->attributes['timeslot'] ?? null)
            ? new Timeslot(json_decode($this->attributes['timeslot']))
            : null;
    }

    protected function setTimeslotAttribute(?Timeslot $timeslot)
    {
        $this->attributes['timeslot'] = $timeslot?->toJson();
    }

    protected function setStatusAttribute(DeliveryStatusEnum|int $value)
    {
        $status = is_int($value) ? DeliveryStatusEnum::from($value) : $value;

        if ($this->status && $this->status != $status) {
            if (!in_array($status, DeliveryStatus::getAllowedNext($this))) {
                $newStatus = new DeliveryStatus($status);
                $oldStatus = new DeliveryStatus($this->status);

                throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
            }

            $this->loadMissing('order');
            if (in_array($this->order->status, OrderStatus::waitProcess())) {
                $orderStatus = new OrderStatus($this->order->status);

                throw new ValidateException("Статус отправления нельзя изменить, когда статус заказа \"{$orderStatus->name}\"");
            }
        }
        $this->attributes['status'] = $status->value;
    }
}
