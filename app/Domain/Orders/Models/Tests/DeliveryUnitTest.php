<?php

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit', 'delivery');

test("Delivery status update success", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->forApprovedOrder()->create(['status' => DeliveryStatusEnum::NEW]);
    $newStatus = DeliveryStatus::getAllowedNext($delivery)[0];
    $delivery->status = $newStatus;
    assertEquals($delivery->status, $newStatus);
});

test("Delivery status update bad: non-allowed", function () {
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->forApprovedOrder()->create(['status' => DeliveryStatusEnum::NEW]);

    $statuses = array_column(DeliveryStatusEnum::cases(), 'value');
    $allowed = array_map(fn (DeliveryStatusEnum $status) => $status->value, DeliveryStatus::getAllowedNext($delivery));
    $badStatuses = array_diff($statuses, $allowed);

    $delivery->status = DeliveryStatusEnum::from(end($badStatuses));
})->throws(ValidateException::class);

test("Delivery status update bad: order-status", function () {
    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::NEW]);
    /** @var Delivery $delivery */
    $delivery = Delivery::factory()->for($order)->create(['status' => DeliveryStatusEnum::NEW]);

    $newStatus = DeliveryStatus::getAllowedNext($delivery)[0];
    $delivery->status = $newStatus;
})->throws(ValidateException::class);
