<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use Ensi\LaravelTestFactories\BaseModelFactory;

class OrderItemFactory extends BaseModelFactory
{
    protected $model = OrderItem::class;

    public function definition(): array
    {
        $pricePerOne = $this->faker->numberBetween(1, 10000);
        $costPerOne = $this->faker->numberBetween($pricePerOne, $pricePerOne + 1000);
        $qty = $this->faker->randomFloat(2, 1, 100);

        return [
            'offer_id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->text(50),
            'qty' => $qty,
            'old_qty' => $this->faker->nullable()->randomFloat(2, 1, 100),
            'price_per_one' => $pricePerOne,
            'cost_per_one' => $costPerOne,
            'product_weight' => $this->faker->numberBetween(1, 100),
            'product_weight_gross' => $this->faker->numberBetween(1, 100),
            'product_width' => $this->faker->numberBetween(1, 100),
            'product_height' => $this->faker->numberBetween(1, 100),
            'product_length' => $this->faker->numberBetween(1, 100),
            'product_barcode' => $this->faker->nullable()->numerify('############'),
            'order_id' => Order::factory(),
            'shipment_id' => Shipment::factory(),
            'is_added' => $this->faker->boolean(),
            'is_deleted' => $this->faker->boolean(),
        ];
    }

    public function isInit(): self
    {
        return $this->state([
            'old_qty' => null,
            'is_added' => false,
            'is_deleted' => false,
        ]);
    }
}
