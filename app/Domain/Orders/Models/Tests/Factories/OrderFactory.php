<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Data\PaymentSystem as PaymentSystemEnumInfo;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;

class OrderFactory extends BaseModelFactory
{
    protected $model = Order::class;

    public function definition()
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $status = $this->faker->randomEnum(OrderStatusEnum::cases());

        $paymentSystem = $this->faker->randomElement(PaymentSystemEnum::cases());
        $paymentMethod = PaymentSystem::getMethodById($paymentSystem);

        $deliveryPrice = $this->faker->numberBetween(0, 200);
        $price = $this->faker->numberBetween($deliveryPrice + 10, 10000);

        if ($status == OrderStatusEnum::WAIT_PAY) {
            $paymentStatus = PaymentStatusEnum::NOT_PAID;
        } elseif (in_array($status, [OrderStatusEnum::NEW, OrderStatusEnum::APPROVED, OrderStatusEnum::DONE])) {
            $paymentStatus = $this->faker->randomEnum([
                PaymentStatusEnum::HOLD,
                PaymentStatusEnum::PAID,
            ]);
        } else {
            $paymentStatus = $this->faker->randomElement([
                PaymentStatusEnum::RETURNED,
                PaymentStatusEnum::TIMEOUT,
                PaymentStatusEnum::CANCELED,
            ]);
        }

        $isPaymentStart = $paymentMethod == PaymentMethodEnum::ONLINE && (in_array($paymentStatus, [PaymentStatusEnum::PAID, PaymentStatusEnum::HOLD, PaymentStatusEnum::RETURNED,]) || $this->faker->boolean());

        return [
            'customer_id' => $this->faker->modelId(),
            'customer_email' => $this->faker->email(),
            'number' => $this->faker->unique()->numerify('######'),
            'price' => $price,
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'spent_bonus' => $this->faker->numberBetween(0, 10),
            'added_bonus' => $this->faker->numberBetween(0, 10),
            'promo_code' => $this->faker->nullable()->word(),
            'delivery_comment' => $this->faker->nullable()->text(50),
            'status' => $status,
            'is_problem' => $this->faker->boolean(),
            'problem_comment' => $this->faker->text(50),
            'is_changed' => $this->faker->boolean(),
            'is_expired' => $this->faker->boolean(),
            'is_return' => $this->faker->boolean(),
            'is_partial_return' => $this->faker->boolean(),
            'client_comment' => $this->faker->nullable()->text(50),
            'delivery_method' => $deliveryMethod,
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_address' => $isDelivery ? DeliveryAddress::factory()->make() : null,
            'receiver_name' => $this->faker->nullable()->name(),
            'receiver_phone' => $this->faker->nullable()->numerify('+7##########'),
            'receiver_email' => $this->faker->nullable()->email(),
            'payment_status' => $paymentStatus,
            'payed_at' => PaymentStatus::isPaid($paymentStatus) ? $this->faker->date() : null,
            'payed_price' => PaymentStatus::isPaid($paymentStatus) ? $price : null,
            'payment_system' => $paymentSystem,
            'payment_expires_at' => $isPaymentStart ? $this->faker->date() : null,
            'payment_link' => $isPaymentStart ? $this->faker->url() : null,
            'payment_external_id' => $isPaymentStart ? $this->faker->uuid() : null,
            'responsible_id' => $this->faker->nullable()->modelId(),
            'source' => $this->faker->randomEnum(OrderSourceEnum::cases()),
        ];
    }

    public function withoutPaymentStart(OrderStatusEnum $orderStatus = OrderStatusEnum::WAIT_PAY): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::NOT_PAID,
            'payment_system' => PaymentSystemEnum::TEST,
            'payed_at' => null,
            'payed_price' => null,
            'payment_expires_at' => null,
            'payment_link' => null,
            'payment_external_id' => null,
        ]);
    }

    public function withPaymentStart(OrderStatusEnum $orderStatus = OrderStatusEnum::WAIT_PAY): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::NOT_PAID,
            'payment_system' => PaymentSystemEnum::TEST,
            'payed_at' => null,
            'payed_price' => null,
            'payment_expires_at' => $this->faker->date(),
            'payment_link' => $this->faker->url(),
            'payment_external_id' => $this->faker->uuid(),
        ]);
    }

    public function withHolded(OrderStatusEnum $orderStatus = OrderStatusEnum::NEW): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::HOLD,
            'payed_price' => fn ($fields) => $fields['price'],
        ]);
    }

    public function withPayed(?OrderStatusEnum $orderStatus = OrderStatusEnum::DONE): self
    {
        return $this->state([
            'status' => $orderStatus,
            'payment_status' => PaymentStatusEnum::PAID,
            'payed_at' => $this->faker->date(),
            'payed_price' => fn ($fields) => $fields['price'],
        ]);
    }

    public function withDeliveryAddress(): self
    {
        return $this->state([
            'delivery_address' => DeliveryAddress::factory()->make(),
        ]);
    }

    public function withPositiveDeliveryPrice(): self
    {
        $deliveryPrice = $this->faker->numberBetween(1, 200);

        return $this->state([
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
        ]);
    }

    public function withSber(): self
    {
        return $this->state([
            'payment_system' => PaymentSystemEnum::SBER,
            'payment_expires_at' => $this->faker->date(),
            'payment_link' => $this->faker->url(),
            'payment_external_id' => $this->faker->uuid(),
        ]);
    }

    public function withCash(): self
    {
        return $this->state([
            'payment_system' => PaymentSystemEnum::CASH,
            'payment_expires_at' => null,
            'payment_link' => null,
            'payment_external_id' => null,
        ]);
    }

    public function withLocalPaymentSystem(): self
    {
        return $this->state([
            'payment_system' => PaymentSystemEnum::TEST,
            'payment_expires_at' => $this->faker->date(),
            'payment_link' => $this->faker->url(),
            'payment_external_id' => $this->faker->uuid(),
        ]);
    }

    public function withTinkoff(): self
    {
        return $this->state([
            'payment_system' => PaymentSystemEnum::TINKOFF,
            'payment_expires_at' => $this->faker->date(),
            'payment_link' => $this->faker->url(),
            'payment_external_id' => $this->faker->uuid(),
        ]);
    }

    public function withPaymentExternalId(): self
    {
        return $this->state(['payment_external_id' => $this->faker->uuid()]);
    }

    public function withPersonalData(): self
    {
        return $this->state([
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),
            'delivery_address' => DeliveryAddress::factory()->make(),
        ]);
    }

    public function withEditableFlag(): self
    {
        $paymentMethod = $this->faker->randomEnum(PaymentMethodEnum::cases());

        return $this->state([
            'status' => $this->faker->randomEnum(OrderStatus::editable()),
            'payment_system' => $paymentMethod == PaymentMethodEnum::ONLINE ? $this->faker->randomEnum(PaymentSystemEnumInfo::editable()) : PaymentSystemEnum::CASH,
            'payment_method' => $paymentMethod,
        ]);
    }
}
