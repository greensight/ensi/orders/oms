<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\ShipmentStatus;
use App\Domain\Orders\Models\Tests\Factories\ShipmentFactory;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\ShipmentStatusEnum;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Shipment
 * @package App\Models\Delivery
 * @property int $id идентификатор отгрузки
 * @property int $delivery_id ид отправления
 * @property int $store_id ид склада
 * @property string $number номер отгрузки
 * @property ShipmentStatusEnum $status статус отгрузки
 * @property CarbonInterface $status_at дата установки статуса
 *
 * @property int $cost сумма товаров отгрузки (рассчитывается автоматически) (коп.)
 * @property int $width ширина (рассчитывается автоматически)
 * @property int $height высота (рассчитывается автоматически)
 * @property int $length длина (рассчитывается автоматически)
 * @property int $weight вес (рассчитывается автоматически)
 *
 * @property CarbonInterface|null $created_at дата создания
 * @property CarbonInterface|null $updated_at дата обновления
 *
 * @property-read Delivery $delivery отправление
 * @property-read Collection|OrderItem[] $orderItems
 */
class Shipment extends Model implements Auditable
{
    use SupportsAudit;

    public const FILLABLE = [
        'status',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'shipments';

    protected $casts = [
        'status' => ShipmentStatusEnum::class,

        'status_at' => 'datetime',
    ];

    /**
     * @param string $deliveryNumber - номер отгрузки
     * @param int $count - Кол-во существующих отправленией
     * @return string
     */
    public static function makeNumber(string $deliveryNumber, int $count): string
    {
        return $deliveryNumber . '-' . sprintf("%'02d", $count + 1);
    }

    public static function factory(): ShipmentFactory
    {
        return ShipmentFactory::new();
    }

    public function delivery(): BelongsTo
    {
        return $this->belongsTo(Delivery::class);
    }

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function isCanceled(): bool
    {
        return in_array($this->status, ShipmentStatus::cancelled());
    }

    protected function setStatusAttribute(ShipmentStatusEnum|int $value)
    {
        $status = is_int($value) ? ShipmentStatusEnum::from($value) : $value;
        $currentStatus = $this->attributes['status'] ?? null;
        if ($currentStatus && $this->status != $status && !in_array($status, ShipmentStatus::getAllowedNext($this))) {
            $newStatus = new ShipmentStatus($status);
            $oldStatus = new ShipmentStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status->value;
    }

    /**
     * @return Collection<OrderItem>
     */
    public function getRealOrderItems(): Collection
    {
        return $this->orderItems->filter(fn (OrderItem $i) => $i->getRealQty() > 0);
    }
}
