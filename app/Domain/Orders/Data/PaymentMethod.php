<?php

namespace App\Domain\Orders\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;

class PaymentMethod
{
    public string $name;

    public function __construct(public PaymentMethodEnum $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById(): void
    {
        $this->name = match ($this->id) {
            PaymentMethodEnum::ONLINE => 'Онлайн',
            PaymentMethodEnum::OFFLINE => 'Оффлайн',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];

        foreach (PaymentMethodEnum::cases() as $method) {
            $all[] = new static($method);
        }

        return $all;
    }
}
