<?php

namespace App\Domain\Orders\Data\Tests\Factories;

use App\Domain\Orders\Data\Timeslot;
use Ensi\LaravelTestFactories\Factory;

class TimeslotFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'from' => $this->faker->time('H:i'),
            'to' => $this->faker->time('H:i'),
        ];
    }

    public function make(array $extra = []): Timeslot
    {
        return new Timeslot($this->makeArray($extra));
    }
}
