<?php

namespace App\Domain\Orders\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;

class OrderSource
{
    public string $name;

    public function __construct(public OrderSourceEnum $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById(): void
    {
        $this->name = match ($this->id) {
            OrderSourceEnum::SITE => 'Сайт',
            OrderSourceEnum::APP => 'Приложение',
            OrderSourceEnum::ADMIN => 'Админ',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];

        foreach (OrderSourceEnum::cases() as $source) {
            $all[] = new static($source);
        }

        return $all;
    }
}
