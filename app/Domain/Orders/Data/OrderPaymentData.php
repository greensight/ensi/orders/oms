<?php

namespace App\Domain\Orders\Data;

use Illuminate\Support\Fluent;

/**
 * @property string $qrCode Путь до qr-кода на оплату
 */
class OrderPaymentData extends Fluent
{
}
