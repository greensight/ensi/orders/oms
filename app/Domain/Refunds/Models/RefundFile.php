<?php

namespace App\Domain\Refunds\Models;

use App\Domain\Refunds\Models\Tests\Factories\RefundFileFactory;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int    $id Идентификатор вложения для заявки на возврат
 * @property string $path Путь до файла
 * @property int    $refund_id Идентификатор заявки на возврат
 * @property string $original_name Оригинальное название файла
 * @property CarbonInterface $created_at Дата создания
 * @property CarbonInterface $updated_at Дата обновления
 */
class RefundFile extends Model implements Auditable
{
    use SupportsAudit;

    public static function factory(): RefundFileFactory
    {
        return RefundFileFactory::new();
    }

    public function refund(): BelongsTo
    {
        return $this->belongsTo(Refund::class);
    }
}
