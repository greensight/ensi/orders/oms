<?php

namespace App\Domain\Refunds\Models\Tests\Factories;

use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Models\Refund;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class RefundFactory extends BaseModelFactory
{
    protected $model = Refund::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'manager_id' => $this->faker->nullable()->modelId(),
            'responsible_id' => $this->faker->nullable()->modelId(),
            'source' => $this->faker->randomEnum(OrderSourceEnum::cases()),
            'status' => $this->faker->randomEnum(RefundStatusEnum::cases()),
            'price' => $this->faker->numberBetween(1, 10000),
            'is_partial' => $this->faker->boolean(),
            'user_comment' => $this->faker->text(100),
            'rejection_comment' => $this->faker->nullable()->text(100),
        ];
    }
}
