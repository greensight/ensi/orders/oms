<?php

namespace App\Domain\Refunds\Models\Tests\Factories;

use App\Domain\Refunds\Models\RefundFile;
use Ensi\LaravelTestFactories\BaseModelFactory;

class RefundFileFactory extends BaseModelFactory
{
    protected $model = RefundFile::class;

    public function definition()
    {
        return [
            'refund_id' => RefundFile::factory(),
            'path' => 'refunds/file.png',
            'original_name' => 'file.png',
        ];
    }

    public function withPath(string $path)
    {
        return $this->state(fn (array $attributes) => [
            'path' => $path,
            'original_name' => pathinfo($path, PATHINFO_FILENAME),
        ]);
    }
}
