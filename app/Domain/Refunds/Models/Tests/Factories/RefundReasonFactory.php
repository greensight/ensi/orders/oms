<?php

namespace App\Domain\Refunds\Models\Tests\Factories;

use App\Domain\Refunds\Models\RefundReason;
use Ensi\LaravelTestFactories\BaseModelFactory;

class RefundReasonFactory extends BaseModelFactory
{
    protected $model = RefundReason::class;

    public function definition()
    {
        return [
            'code' => $this->faker->text(20),
            'name' => $this->faker->text(255),
            'description' => $this->faker->nullable()->text(),
        ];
    }
}
