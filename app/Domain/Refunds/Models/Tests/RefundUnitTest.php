<?php

use App\Domain\Refunds\Data\RefundStatus;
use App\Domain\Refunds\Models\Refund;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Refund status update success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Refund $refund */
    $refund = Refund::factory()->create(['status' => RefundStatusEnum::NEW]);
    $newStatus = RefundStatus::getAllowedNext($refund)[0];
    $refund->status = $newStatus;

    assertEquals($refund->status, $newStatus);
})->with(FakerProvider::$optionalDataset);

test("Refund status update bad", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Refund $refund */
    $refund = Refund::factory()->create(['status' => RefundStatusEnum::CANCELED]);
    $statuses = array_column(RefundStatusEnum::cases(), 'value');
    $allowed = array_map(fn (RefundStatusEnum $status) => $status->value, RefundStatus::getAllowedNext($refund));

    $badStatuses = array_diff($statuses, $allowed);
    $refund->status = RefundStatusEnum::from(current($badStatuses));
})->with(FakerProvider::$optionalDataset)
    ->throws(ValidateException::class);
