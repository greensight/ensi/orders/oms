<?php

namespace App\Domain\Refunds\Actions\Refund;

use App\Domain\Refunds\Models\RefundFile;
use App\Exceptions\ValidateException;
use Illuminate\Database\Eloquent\Collection;

class DeleteRefundFilesAction
{
    public function execute(int $refundId, array $ids): void
    {
        /** @var Collection|RefundFile[] $files */
        $files = RefundFile::query()->whereIn('id', $ids)->get();

        $badCount = $files->filter(fn (RefundFile $file) => $file->refund_id != $refundId)->count();
        if ($badCount > 0) {
            throw new ValidateException("Не все файлы относятся к указанному заказу");
        }

        foreach ($files as $file) {
            $file->delete();
        }
    }
}
