<?php

namespace App\Domain\Refunds\Actions\RefundReason;

use App\Domain\Refunds\Models\RefundReason;

class CreateRefundReasonAction
{
    public function execute(array $fields): RefundReason
    {
        $reason = new RefundReason();
        $reason->code = $fields['code'];
        $reason->name = $fields['name'];
        $reason->description = $fields['description'] ?? null;
        $reason->save();

        return $reason;
    }
}
