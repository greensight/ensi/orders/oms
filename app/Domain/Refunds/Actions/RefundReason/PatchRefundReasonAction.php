<?php

namespace App\Domain\Refunds\Actions\RefundReason;

use App\Domain\Refunds\Models\RefundReason;

class PatchRefundReasonAction
{
    public function execute(int $id, array $fields): RefundReason
    {
        /** @var RefundReason $reason */
        $reason = RefundReason::query()->findOrFail($id);
        $reason->update($fields);
        $reason->setRelations([]);

        return $reason;
    }
}
