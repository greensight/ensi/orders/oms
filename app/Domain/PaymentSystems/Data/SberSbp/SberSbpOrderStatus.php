<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

enum SberSbpOrderStatus: string
{
    case PAID = 'PAID';
    case CREATED = 'CREATED';
    case REVERSED = 'REVERSED';
    case REFUNDED = 'REFUNDED';
    case REVOKED = 'REVOKED';
    case DECLINED = 'DECLINED';
    case EXPIRED = 'EXPIRED';
    case AUTHORIZED = 'AUTHORIZED';
    case CONFIRMED = 'CONFIRMED';
    case ON_PAYMENT = 'ON_PAYMENT';
}
