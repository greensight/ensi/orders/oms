<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CreateSberSbpOrderRequest implements SberSbpRequest
{
    public string $orderNumber;
    public int $orderSum;
    public string $currency;
    public string $memberId;
    public string $idQr;
    public string $orderCreateDate;
    public string $sbpMemberId;

    public function toArray(): array
    {
        return [
            'order_number' => $this->orderNumber,
            'order_sum' => $this->orderSum,
            'currency' => $this->currency,
            'member_id' => $this->memberId,
            'id_qr' => $this->idQr,
            'order_create_date' => $this->orderCreateDate,
            'sbp_member_id' => $this->sbpMemberId,
        ];
    }
}
