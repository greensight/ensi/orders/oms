<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CreateSberSbpOrderResponse
{
    public ?string $errorDescription;
    public ?string $orderNumber;
    public ?string $orderFormUrl;
    public string $errorCode;
    public ?string $orderId;
    public ?string $orderState;

    public function __construct(array $response)
    {
        $this->errorDescription = $response['error_description'] ?? null;
        $this->orderNumber = $response['order_number'] ?? null;
        $this->orderFormUrl = $response['order_form_url'] ?? null;
        $this->errorCode = $response['error_code'];
        $this->orderId = $response['order_id'] ?? null;
        $this->orderState = $response['order_state'] ?? null;
    }

    public function isValid(): bool
    {
        return filled($this->orderId) && filled($this->orderFormUrl);
    }
}
