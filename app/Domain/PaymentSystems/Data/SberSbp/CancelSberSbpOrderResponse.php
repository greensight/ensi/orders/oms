<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CancelSberSbpOrderResponse
{
    public ?string $operationDateTime;
    public ?string $operationType;
    public ?string $operationCurrency;
    public ?int $operationSum;
    public ?string $operationId;
    public ?string $orderId;
    public ?string $orderState;
    public string $errorCode;
    public ?string $errorDescription;

    public function __construct(array $response)
    {
        $this->operationDateTime = $response['operation_date_time'] ?? null;
        $this->operationType = $response['operation_type'] ?? null;
        $this->operationCurrency = $response['operation_currency'] ?? null;
        $this->operationSum = $response['operation_sum'] ?? null;
        $this->operationId = $response['operation_id'] ?? null;
        $this->orderId = $response['order_id'] ?? null;
        $this->orderState = $response['order_status'] ?? null;
        $this->errorCode = $response['error_code'];
        $this->errorDescription = $response['error_description'] ?? null;
    }

    public function isValid(): bool
    {
        return filled($this->orderState);
    }
}
