<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class RevokeSberSbpOrderResponse
{
    public ?string $orderId;
    public ?string $orderState;
    public string $errorCode;
    public ?string $errorDescription;

    public function __construct(array $response)
    {
        $this->orderId = $response['order_id'] ?? null;
        $this->orderState = $response['order_state'] ?? null;
        $this->errorCode = $response['error_code'];
        $this->errorDescription = $response['error_description'] ?? null;
    }

    public function isValid(): bool
    {
        return filled($this->orderState);
    }
}
