<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class GetSberSbpOrderStatusRequest implements SberSbpRequest
{
    public string $orderId;
    public string $partnerOrderNumber;
    public string $tid;

    public function toArray(): array
    {
        return [
            'order_id' => $this->orderId,
            'partner_order_number' => $this->partnerOrderNumber,
            'tid' => $this->tid,
        ];
    }
}
