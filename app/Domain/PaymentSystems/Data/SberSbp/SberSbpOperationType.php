<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

enum SberSbpOperationType: string
{
    case PAY = 'PAY';
    case REFUND = 'REFUND';
}
