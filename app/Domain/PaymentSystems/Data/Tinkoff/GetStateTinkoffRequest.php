<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class GetStateTinkoffRequest implements TinkoffRequest
{
    public string $paymentId;

    public function toArray(): array
    {
        return [
            'PaymentId' => $this->paymentId,
        ];
    }
}
