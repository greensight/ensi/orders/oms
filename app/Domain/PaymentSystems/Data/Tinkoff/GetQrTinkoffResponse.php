<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class GetQrTinkoffResponse extends AbstractTinkoffResponse
{
    public string $orderId;
    public string $paymentId;
    public string $data;

    public function __construct(array $response)
    {
        parent::__construct($response);

        $this->orderId = $response['OrderId'];
        $this->paymentId = $response['PaymentId'];
        $this->data = $response['Data'];
    }
}
