<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class AbstractTinkoffResponse
{
    public bool $success;
    public ?string $errorCode;
    public ?string $message;
    public ?string $details;
    public ?string $qrCancelCode;
    public ?string $qrCancelMessage;

    public function __construct(array $response)
    {
        $this->success = $response['Success'];
        $this->errorCode = $response['ErrorCode'] ?? null;
        $this->message = $response['Message'] ?? null;
        $this->details = $response['Details'] ?? null;
        $this->qrCancelCode = $response['QrCancelCode'] ?? null;
        $this->qrCancelMessage = $response['QrCancelMessage'] ?? null;
    }
}
