<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff\Enums;

enum TinkoffPaymentMethod: string
{
    case FULL_PAYMENT = 'full_payment';
    case FULL_PREPAYMENT = 'full_prepayment';
    case PREPAYMENT = 'prepayment';
    case ADVANCE = 'advance';
    case PARTIAL_PAYMENT = 'partial_payment';
    case CREDIT = 'credit';
    case CREDIT_PAYMENT = 'credit_payment';
}
