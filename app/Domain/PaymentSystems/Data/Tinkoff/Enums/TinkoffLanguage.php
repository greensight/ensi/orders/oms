<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff\Enums;

enum TinkoffLanguage: string
{
    case LANGUAGE_RU = 'ru';
    case LANGUAGE_EN = 'en';
}
