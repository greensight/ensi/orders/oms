<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff\Enums;

enum TinkoffPaymentType: string
{
    case PAY_TYPE_ONE_STAGE = 'O';
    case PAY_TYPE_TWO_STAGE = 'T';
}
