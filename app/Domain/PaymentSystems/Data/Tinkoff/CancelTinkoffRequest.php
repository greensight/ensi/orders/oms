<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class CancelTinkoffRequest implements TinkoffRequest
{
    public string $paymentId;

    public function toArray(): array
    {
        return [
            'PaymentId' => $this->paymentId,
        ];
    }
}
