<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff\Tests;

use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use Ensi\LaravelTestFactories\BaseApiFactory;

class GetOrderStatusExtendedFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'errorCode' => 0,
            'orderStatus' => $this->faker->randomElement($this->statuses()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    private function statuses(): array
    {
        return [
            TinkoffPaymentStatus::STATUS_AUTHORIZED,
            TinkoffPaymentStatus::STATUS_CONFIRMED,
            TinkoffPaymentStatus::STATUS_REVERSED,
            TinkoffPaymentStatus::STATUS_PARTIAL_REVERSED,
            TinkoffPaymentStatus::STATUS_PARTIAL_REFUNDED,
            TinkoffPaymentStatus::STATUS_REFUNDED,
        ];
    }
}
