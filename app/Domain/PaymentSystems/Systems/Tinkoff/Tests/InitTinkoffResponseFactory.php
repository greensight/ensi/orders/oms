<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff\Tests;

use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class InitTinkoffResponseFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'OrderId' => Order::makePaymentNumber($this->faker->modelId()),
            'Amount' => $this->faker->numberBetween(1),
            'Status' => $this->faker->randomEnum(TinkoffPaymentStatus::cases()),
            'PaymentId' => $this->faker->uuid(),
            'PaymentURL' => $this->faker->url(),
            'Success' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): InitTinkoffResponse
    {
        return new InitTinkoffResponse($this->makeArray($extra));
    }
}
