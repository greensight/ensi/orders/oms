<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff;

use App\Domain\PaymentSystems\Systems\PaymentData;

class TinkoffPaymentData extends PaymentData
{
    public ?string $returnLink;
    public ?string $failLink;

    public function __construct(array $data, protected int $fullPrice, public int $deliveryPrice)
    {
        parent::__construct($data, $fullPrice, $deliveryPrice);
        $this->returnLink = $data['returnLink'] ?? null;
        $this->failLink = $data['failLink'] ?? null;
    }

    public function getData(): array
    {
        return [
            'returnLink' => $this->returnLink,
            'failLink' => $this->failLink,
            'amount' => $this->getFullPrice(),
        ];
    }
}
