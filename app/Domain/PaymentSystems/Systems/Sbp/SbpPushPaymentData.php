<?php

namespace App\Domain\PaymentSystems\Systems\Sbp;

use App\Domain\PaymentSystems\Systems\PushPaymentData;

class SbpPushPaymentData extends PushPaymentData
{
    /** @var string */
    public const STATUS_DONE = 'done';

    public $status;

    public function __construct(protected array $data)
    {
        parent::__construct($data);
        $this->externalId = $data['paymentId'];
        $this->status = $data['status'];
    }
}
