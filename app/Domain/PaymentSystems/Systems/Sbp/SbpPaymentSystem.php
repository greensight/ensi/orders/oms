<?php

namespace App\Domain\PaymentSystems\Systems\Sbp;

use App\Domain\Orders\Actions\Payment\PaymentQr\GenerateQrCodeAction;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Exceptions\ValidateException;
use Ramsey\Uuid\Uuid;

/**
 * Class SbpPaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
class SbpPaymentSystem extends PaymentSystem
{
    /**
     * @param SbpPaymentData $paymentData
     * @inheritDoc
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        $uuid = Uuid::uuid1()->toString();
        $paymentData->externalId = $uuid;
        $paymentData->paymentLink = toPublicPath(route('paymentPage', ['paymentId' => $uuid]));
        $paymentData->returnLink = $returnLink;
        $paymentData->handlerUrl = route('handler.localPayment');
        $paymentData->qrCode = $this->getQrCode($paymentData->orderId, $returnLink);
    }

    private function getQrCode(int $orderId, string $paymentLink): string
    {
        $action = resolve(GenerateQrCodeAction::class);

        return $action->execute($orderId, $paymentLink);
    }

    /**
     * @param SbpPaymentData $paymentData
     * @param SbpPushPaymentData $pushPaymentData
     * @inheritDoc
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        if ($pushPaymentData->status == SbpPushPaymentData::STATUS_DONE) {
            $paymentData->setStatusPaid();
        }
    }

    /**
     * @param SbpPaymentData $paymentData
     * @inheritDoc
     */
    public function commitHoldedPayment($paymentData)
    {
        throw new ValidateException("Нельзя произвести списание средств из HOLD в СБП. средства списываются при создании оплаты");
    }

    /**
     * @param SbpPaymentData $paymentData
     * @inheritDoc
     */
    public function revert($paymentData)
    {
        $paymentData->setStatusCancelled();
    }

    public function reinit($paymentData)
    {
        // TODO: Implement reinit() method.
    }
}
