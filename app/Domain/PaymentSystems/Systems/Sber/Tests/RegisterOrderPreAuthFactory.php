<?php

namespace App\Domain\PaymentSystems\Systems\Sber\Tests;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RegisterOrderPreAuthFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'errorCode' => 0,
            'orderId' => $this->faker->uuid(),
            'formUrl' => $this->faker->url(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
