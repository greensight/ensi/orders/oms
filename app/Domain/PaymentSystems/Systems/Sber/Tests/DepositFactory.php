<?php

namespace App\Domain\PaymentSystems\Systems\Sber\Tests;

use Ensi\LaravelTestFactories\BaseApiFactory;

class DepositFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'errorCode' => 0,
            'orderStatus' => "Успешно",
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
