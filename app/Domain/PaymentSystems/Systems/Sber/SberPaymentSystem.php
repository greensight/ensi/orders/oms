<?php

namespace App\Domain\PaymentSystems\Systems\Sber;

use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\OrderStatus;

class SberPaymentSystem extends PaymentSystem
{
    protected LoggerInterface $logger;

    protected const TIMEZONE = '+3';

    protected const DATETIME_FORMAT = 'Y-m-d\TH:i:s';

    protected const LANGUAGE = 'RU';

    protected array $options;

    public function __construct()
    {
        $this->logger = logger()->channel('payments_sber');

        $this->options = [
            'userName' => config('services.sber.login'),
            'password' => config('services.sber.pass'),
            'language' => self::LANGUAGE,
            'currency' => Currency::RUB,
            'apiUri' => Client::API_URI_TEST,
        ];
    }

    /**
     * @param SberPaymentData $paymentData
     * @throws Exception
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        if ($paymentData->returnLink) {
            $this->logger->warning('Ссылка на оплату уже сгенерирована');

            return;
        }

        try {
            $this->logger->info('Регистрация нового заказа в Сбере');

            $paymentData->returnLink = $returnLink;
            $paymentData->failLink = $failLink;

            $response = $this->sendRegisterPreAuth($paymentData);

            $this->logger->info('Ответ от registerOrderPreAuth.do', $response);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе registerOrderPreAuth.do', ['message' => $e->getMessage()]);

            throw $e;
        }

        $paymentData->externalId = $response['orderId'];
        $paymentData->paymentLink = $response['formUrl'];
    }

    /**
     * @param SberPaymentData $paymentData
     * @throws Exception
     */
    public function commitHoldedPayment($paymentData)
    {
        $this->logger->info('Попытка списания захолдированных средств', [
            'payment_external_id' => $paymentData->externalId,
        ]);

        $this->sendDeposit($paymentData);
        $paymentData->setStatusPaid();

        $this->logger->info('Окончание списывания захолдированных средств', [
            'payment_external_id' => $paymentData->externalId,
        ]);
    }

    public function reinit($paymentData)
    {
    }

    /**
     * @param SberPaymentData $paymentData
     * @throws Exception
     */
    public function revert($paymentData)
    {
        try {
            if (in_array($paymentData->status, PaymentStatus::cancelled())) {
                return;
            }

            if ($paymentData->status == PaymentStatusEnum::HOLD) {
                $this->sendReverse($paymentData);
                $paymentData->setStatusCancelled();
            } elseif ($paymentData->status == PaymentStatusEnum::PAID) {
                $this->sendRefund($paymentData);
                $paymentData->setStatusReturned();
            } else {
                $paymentData->setStatusCancelled();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function sendRegisterPreAuth(SberPaymentData $payment): array
    {
        $request = [
            'clientId' => $payment->customerId,
            'expirationDate' => $payment->expiresAt->setTimezone(self::TIMEZONE)->format(self::DATETIME_FORMAT),
        ];

        if ($payment->failLink) {
            $request['failUrl'] = $payment->failLink;
        }

        $orderId = Order::makePaymentNumber($payment->orderId);

        $this->logger->info(
            'Запрос registerPreAuth.do',
            array_merge(
                $request,
                $this->options,
                [
                    'orderNumber' => $orderId,
                    'amount' => $payment->getFullPrice(),
                    'returnUrl' => $payment->returnLink,
                ],
            )
        );

        return $this->client()->registerOrderPreAuth(
            $orderId,
            $payment->getFullPrice(),
            $payment->returnLink,
            $request
        );
    }

    private function sendGetOrderStatusExtended(SberPaymentData $payment): array
    {
        try {
            $this->logger->info('Запрос getOrderStatusExtended.do', array_merge(
                $this->options,
                ['externalId' => $payment->externalId]
            ));

            return $this->client()->getOrderStatus($payment->externalId);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе getOrderStatusExtended.do', ['message' => $e->getMessage()]);

            throw $e;
        }
    }

    /**
     * @param SberPaymentData $paymentData
     *
     * @throws Exception
     */
    public function checkPayment($paymentData): void
    {
        if (!$paymentData->externalId) {
            $msg = 'Не указан payment_external_id';

            $this->logger->error($msg);

            throw new Exception($msg);
        }

        $response = $this->sendGetOrderStatusExtended($paymentData);

        $this->logger->info('Ответ getOrderStatusExtended.do', $response);

        $orderStatus = $response['orderStatus'];

        if (OrderStatus::isApproved($orderStatus)) {
            $paymentData->setStatusHold();
        }

        if (OrderStatus::isDeposited($orderStatus)) {
            $paymentData->setStatusPaid();
        }

        if (OrderStatus::isRefunded($orderStatus)) {
            $paymentData->setStatusReturned();
        }

        if (OrderStatus::isReversed($orderStatus) || OrderStatus::isDeclined($orderStatus)) {
            $paymentData->setStatusCancelled();
        }
    }

    /**
     * @throws Exception
     */
    private function sendReverse(SberPaymentData $payment): void
    {
        try {
            $this->logger->info('Запрос reverse.do', array_merge(
                $this->options,
                ['externalId' => $payment->externalId],
            ));

            $this->client()->reverseOrder($payment->externalId);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе reverse.do', ['message' => $e->getMessage()]);

            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    private function sendRefund(SberPaymentData $payment): void
    {
        try {
            $this->logger->info('Запрос refund.do', array_merge(
                $this->options,
                ['externalId' => $payment->externalId]
            ));

            $this->client()->refundOrder($payment->externalId, 0);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе refund.do', ['message' => $e->getMessage()]);

            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    private function sendDeposit(SberPaymentData $payment): void
    {
        try {
            $this->logger->info('Запрос deposit.do', array_merge(
                $this->options,
                ['externalId' => $payment->externalId]
            ));

            $this->client()->deposit($payment->externalId, $payment->getFullPrice());
        } catch (Exception $e) {
            $this->logger->error('Запрос deposit упал с ошибкой', [
                'message' => $e->getMessage(), 'code' => $e->getCode(),
            ]);

            throw $e;
        }
    }

    private function client(): Client
    {
        return app()->make(Client::class, ['options' => $this->options]);
    }
}
