<?php

namespace App\Domain\PaymentSystems\Systems;

use App\Domain\PaymentSystems\Systems\Local\LocalPaymentSystem;
use App\Domain\PaymentSystems\Systems\Sber\SberPaymentSystem;
use App\Domain\PaymentSystems\Systems\SberSbp\SberSbpPaymentSystem;
use App\Domain\PaymentSystems\Systems\Sbp\SbpPaymentSystem;
use App\Domain\PaymentSystems\Systems\Tinkoff\TinkoffPaymentSystem;
use App\Domain\PaymentSystems\Systems\TinkoffSbp\TinkoffSbpPaymentSystem;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Exception;

/**
 * Interface PaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
abstract class PaymentSystem
{
    public static function getById(PaymentSystemEnum $paymentSystemId): PaymentSystem
    {
        return match ($paymentSystemId) {
            //            PaymentSystemEnum::YANDEX => new YandexPaymentSystem(),
            PaymentSystemEnum::TEST => new LocalPaymentSystem(),
            PaymentSystemEnum::SBP => new SbpPaymentSystem(),
            PaymentSystemEnum::TINKOFF => new TinkoffPaymentSystem(),
            PaymentSystemEnum::TINKOFF_SBP => new TinkoffSbpPaymentSystem(),
            PaymentSystemEnum::SBER => new SberPaymentSystem(),
            PaymentSystemEnum::SBER_SBP => new SberSbpPaymentSystem(),
            default => throw new Exception("Неизвестная система оплаты"),
        };
    }

    public static function getMethodById(PaymentSystemEnum $paymentSystemId): PaymentMethodEnum
    {
        return match ($paymentSystemId) {
            //            PaymentSystemEnum::YANDEX,
            PaymentSystemEnum::TEST, PaymentSystemEnum::SBP, PaymentSystemEnum::TINKOFF, PaymentSystemEnum::TINKOFF_SBP,
            PaymentSystemEnum::SBER, PaymentSystemEnum::SBER_SBP => PaymentMethodEnum::ONLINE,
            PaymentSystemEnum::CASH => PaymentMethodEnum::OFFLINE,
            //            default => throw new Exception("Неизвестная система оплаты"),
        };
    }

    /**
     * Обратиться к внешней системе оплаты для создания платежа
     * @param PaymentData $paymentData
     * @param string $returnLink ссылка на страницу, на которую пользователь должен попасть после оплаты
     * @param string|null $failLink ссылка на страницу, на которую пользователь должен попасть  вслучае ошибки
     */
    abstract public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void;

    /**
     * Провести оплату холдированными средствами.
     * @param PaymentData $paymentData
     */
    abstract public function commitHoldedPayment($paymentData);

    /**
     * Пересоздание оплаты, после обновление стоимости заказа
     * @param PaymentData $paymentData
     * @return mixed
     */
    abstract public function reinit($paymentData);

    /**
     * Отменить оплату.
     * @param PaymentData $paymentData
     */
    abstract public function revert($paymentData);

    /**
     * Получить данные от платёжной системы
     * @param PaymentData $paymentData
     */
    public function checkPayment($paymentData): void
    {
        throw new Exception("Система оплаты не поддерживает rest");
    }

    /**
     * Обработать данные от платёжной системы о совершении платежа
     * @param PaymentData $paymentData
     * @param PushPaymentData $pushPaymentData
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        throw new Exception("Система оплаты не поддерживает push");
    }
}
