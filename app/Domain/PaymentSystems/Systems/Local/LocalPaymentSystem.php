<?php

namespace App\Domain\PaymentSystems\Systems\Local;

use App\Domain\PaymentSystems\Systems\PaymentSystem;
use Ramsey\Uuid\Uuid;

/**
 * Class LocalPaymentSystem
 * @package App\Domain\PaymentSystems\Systems
 */
class LocalPaymentSystem extends PaymentSystem
{
    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        $uuid = Uuid::uuid1()->toString();
        $paymentData->externalId = $uuid;
        $paymentData->paymentLink = toPublicPath(route('paymentPage', ['paymentId' => $uuid]));
        $paymentData->returnLink = $returnLink;
        $paymentData->handlerUrl = route('handler.localPayment');
    }

    /**
     * @param LocalPaymentData $paymentData
     * @param LocalPushPaymentData $pushPaymentData
     * @inheritDoc
     */
    public function handlePushPayment($paymentData, $pushPaymentData): void
    {
        if ($pushPaymentData->status == LocalPushPaymentData::STATUS_DONE) {
            $paymentData->setStatusHold();
        }
    }

    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function commitHoldedPayment($paymentData)
    {
        $paymentData->setStatusPaid();
    }

    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function checkPayment($paymentData): void
    {
        return;
    }

    /**
     * @param LocalPaymentData $paymentData
     * @inheritDoc
     */
    public function revert($paymentData)
    {
        $paymentData->setStatusCancelled();
    }

    public function reinit($paymentData)
    {
        // TODO: Implement reinit() method.
    }
}
