<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp;

use App\Domain\PaymentSystems\Data\SberSbp\CancelSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\CancelSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\CreateSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\CreateSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\GetSberSbpOrderStatusRequest;
use App\Domain\PaymentSystems\Data\SberSbp\GetSberSbpOrderStatusResponse;
use App\Domain\PaymentSystems\Data\SberSbp\OrderOperationParams;
use App\Domain\PaymentSystems\Data\SberSbp\RevokeSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\RevokeSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOperationType;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpRequest;
use App\Exceptions\ValidateException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class SberSbpClient
{
    public const CURRENCY_RUB = '643';
    public const DATE_TIME_FORMAT = 'Y-m-d\TH:i:s\Z';
    public const GRANT_CREDENTIALS = 'client_credentials';

    // Идентификатор банка-участника "ПАО СберБанк" в СБП.
    public const SBP_MEMBER_ID = '100000000111';

    private const SPB_CODE_SUCCESS = '000000';
    public const SPB_CODE_IN_PROCESS = '990000';

    private ClientInterface $client;

    private LoggerInterface $logger;

    private string $oauthUrl;

    private string $clientId;
    private string $clientSecret;
    private string $grantType;
    private string $clientCertPath;

    private string $scopeCreate;
    private string $scopeStatus;
    private string $scopeRevoke;
    private string $scopeCancel;

    private string $idQr;
    private string $terminalId;

    public function __construct(ClientInterface $client = null)
    {
        $this->client = $client ?: new Client();
        $this->logger = logger()->channel('payments_sber_sbp');

        $this->oauthUrl = config('services.sber.qr.oauth_url');

        $this->clientId = config('services.sber.qr.client_id');
        $this->clientSecret = config('services.sber.qr.client_secret');
        $this->grantType = self::GRANT_CREDENTIALS;

        $this->clientCertPath = config('services.sber.qr.cert_path') ?: storage_path('sber');

        $this->scopeCreate = config('services.sber.qr.scope.create');
        $this->scopeStatus = config('services.sber.qr.scope.status');
        $this->scopeRevoke = config('services.sber.qr.scope.revoke');
        $this->scopeCancel = config('services.sber.qr.scope.cancel');

        $this->idQr = config('services.sber.qr.id_qr');
        $this->terminalId = config('services.sber.qr.terminal_id');
    }

    public function create(CreateSberSbpOrderRequest $request): CreateSberSbpOrderResponse
    {
        $token = $this->getToken($this->scopeCreate);

        $request->currency = self::CURRENCY_RUB;
        $request->idQr = $this->idQr;
        $request->sbpMemberId = self::SBP_MEMBER_ID;

        $result = $this->send($token, 'creation', $request);

        return new CreateSberSbpOrderResponse($result);
    }

    public function getStatus(GetSberSbpOrderStatusRequest $request): GetSberSbpOrderStatusResponse
    {
        $token = $this->getToken($this->scopeStatus);

        $request->tid = $this->terminalId;

        $result = $this->send($token, 'status', $request);

        return new GetSberSbpOrderStatusResponse($result);
    }

    public function cancel(CancelSberSbpOrderRequest $request, string $orderNumber): CancelSberSbpOrderResponse
    {
        $token = $this->getToken($this->scopeCancel);

        $statusRequest = new GetSberSbpOrderStatusRequest();
        $statusRequest->orderId = $request->orderId;
        $statusRequest->partnerOrderNumber = $orderNumber;
        $statusRequest->tid = $this->terminalId;

        /** @var OrderOperationParams $operationParams */
        $operationParams = $this->getStatus($statusRequest)->orderOperationParams
            ->where('operationType', SberSbpOperationType::PAY->value)
            ->first();

        $request->idQr = $this->idQr;
        $request->tid = $this->terminalId;
        $request->operationId = $operationParams->operationId;
        $request->authCode = $operationParams->authCode;
        $request->operationType = SberSbpOperationType::REFUND;
        $request->operationCurrency = self::CURRENCY_RUB;

        $result = $this->send($token, 'cancel', $request);

        return new CancelSberSbpOrderResponse($result);
    }

    public function revoke(RevokeSberSbpOrderRequest $request): RevokeSberSbpOrderResponse
    {
        $token = $this->getToken($this->scopeRevoke);

        $result = $this->send($token, 'revocation', $request);

        return new RevokeSberSbpOrderResponse($result);
    }

    private function getToken(string $scope): string
    {
        $response = $this->client->request('POST', $this->oauthUrl, [
            'form_params' => [
                'grant_type' => $this->grantType,
                'scope' => $scope,
            ],
            'cert' => $this->clientCertPath('client_cert.crt'),
            'ssl_key' => $this->clientCertPath('private.key'),
            'headers' => [
                'RqUID' => $this->requestId(),
                'Accept' => 'application/json',
            ],
            'auth' => [$this->clientId, $this->clientSecret],
        ]);

        return json_decode($response->getBody(), true)['access_token'];
    }

    private function send(string $token, string $method, SberSbpRequest $request): array
    {
        $data = $request->toArray();

        $requestId = $this->requestId();
        $data['rq_uid'] = $requestId;
        $data['rq_tm'] = now()->format(self::DATE_TIME_FORMAT);

        $this->logger->info("Request start $method", $data);

        $result = $this->client->request('POST', $method, [
            'cert' => $this->clientCertPath('client_cert.crt'),
            'ssl_key' => $this->clientCertPath('private.key'),
            'headers' => [
                'RqUID' => $requestId,
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token",
            ],
            'json' => $data,
        ]);

        $data = json_decode($result->getBody(), true);

        $this->logger->info("Request finish $method", $data);

        if (!in_array($data['error_code'], [self::SPB_CODE_SUCCESS, self::SPB_CODE_IN_PROCESS])) {
            throw new ValidateException($data['error_description'] ?? 'Received error from Sber SBP');
        }

        return $data;
    }

    private function clientCertPath(string $file): string
    {
        $filePath = rtrim($this->clientCertPath, '/\\') . DIRECTORY_SEPARATOR . $file;

        if (!file_exists($filePath)) {
            throw new Exception("$filePath not found");
        }

        return $filePath;
    }

    private function requestId(): string
    {
        return str_replace('-', '', Uuid::uuid4()->toString());
    }
}
