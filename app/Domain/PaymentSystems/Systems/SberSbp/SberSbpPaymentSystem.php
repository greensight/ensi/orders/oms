<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp;

use App\Domain\Orders\Actions\Payment\PaymentQr\GenerateQrCodeAction;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Data\SberSbp\CancelSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\CreateSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\GetSberSbpOrderStatusRequest;
use App\Domain\PaymentSystems\Data\SberSbp\RevokeSberSbpOrderRequest;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOrderStatus;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;

class SberSbpPaymentSystem extends PaymentSystem
{
    protected LoggerInterface $logger;
    protected SberSbpClient $client;
    protected GenerateQrCodeAction $generateQrCodeAction;

    public function __construct()
    {
        $this->client = resolve(SberSbpClient::class);
        $this->generateQrCodeAction = resolve(GenerateQrCodeAction::class);
        $this->logger = logger()->channel('payments_sber_sbp');
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        if ($paymentData->qrCode) {
            $this->logger->warning('QR code is already generated');

            return;
        }

        try {
            $paymentData->returnLink = $returnLink;

            $orderNumber = Order::makePaymentNumber($paymentData->orderId);

            $request = new CreateSberSbpOrderRequest();
            $request->orderNumber = $orderNumber;
            $request->orderSum = $paymentData->getFullPrice();
            $request->orderCreateDate = $paymentData->orderDate->format(SberSbpClient::DATE_TIME_FORMAT);
            $request->memberId = (string)$paymentData->customerId;

            $response = $this->client->create($request);
        } catch (Exception $e) {
            $this->logger->error('Error occurred while registering new payment in Sber SBP', ['message' => $e->getMessage()]);

            throw $e;
        }

        if (!$response->isValid()) {
            $this->logger->error('Error occurred while registering new payment in Sber SBP', ['message' => 'Invalid response']);

            throw new ValidateException('Received invalid response from Sber SBP');
        }

        $paymentData->externalId = $response->orderId;
        $paymentData->paymentLink = $response->orderFormUrl;
        $paymentData->qrCode = $this->generateQrCodeAction->execute($paymentData->orderId, $response->orderFormUrl);
    }

    public function commitHoldedPayment($paymentData)
    {
        throw new ValidateException("Cannot hold or commit payment in SBP");
    }

    public function reinit($paymentData)
    {
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function revert($paymentData)
    {
        if (in_array($paymentData->status, PaymentStatus::cancelled())) {
            return;
        }

        try {
            if ($paymentData->status == PaymentStatusEnum::NOT_PAID) {
                $request = new RevokeSberSbpOrderRequest();
                $request->orderId = $paymentData->externalId;

                $response = $this->client->revoke($request);
            } elseif ($paymentData->status == PaymentStatusEnum::PAID) {
                $orderNumber = Order::makePaymentNumber($paymentData->orderId);

                $request = new CancelSberSbpOrderRequest();
                $request->orderId = $paymentData->externalId;
                $request->cancelOperationSum = $paymentData->getFullPrice();

                $response = $this->client->cancel($request, $orderNumber);
            } else {
                $paymentData->setStatusCancelled();

                return;
            }
        } catch (Exception $e) {
            $this->logger->error('Error occurred while reverting payment in Sber SBP', ['message' => $e->getMessage()]);

            throw $e;
        }

        if ($response->errorCode === $this->client::SPB_CODE_IN_PROCESS) {
            $this->logger->info('Warning in Sber SBP', ['message' => 'Operation is in process']);
        }

        if (!$response->isValid()) {
            $this->logger->error('Error occurred while reverting payment status in Sber SBP', ['message' => 'Invalid response']);

            throw new ValidateException('Received invalid response from Sber SBP');
        }

        if (in_array($response->orderState, [SberSbpOrderStatus::REVERSED->value, SberSbpOrderStatus::REVOKED->value])) {
            $paymentData->setStatusCancelled();
        }

        if ($response->orderState == SberSbpOrderStatus::REFUNDED->value) {
            $paymentData->setStatusReturned();
        }
    }

    /**
     * @param SberSbpPaymentData $paymentData
     * @throws Exception
     */
    public function checkPayment($paymentData): void
    {
        if (!$paymentData->qrCode) {
            $msg = 'QR code not generated';

            $this->logger->error($msg);

            throw new Exception($msg);
        }

        $orderNumber = Order::makePaymentNumber($paymentData->orderId);

        $request = new GetSberSbpOrderStatusRequest();
        $request->orderId = $paymentData->externalId;
        $request->partnerOrderNumber = $orderNumber;

        $response = $this->client->getStatus($request);

        if (!$response->isValid()) {
            $this->logger->error('Error occurred while checking payment status in Sber SBP', ['message' => 'Invalid response']);

            throw new ValidateException('Received invalid response from Sber SBP');
        }

        if ($response->orderState == SberSbpOrderStatus::PAID->value) {
            $paymentData->setStatusPaid();
        }

        if (in_array($response->orderState, [SberSbpOrderStatus::REVERSED->value, SberSbpOrderStatus::REVOKED->value])) {
            $paymentData->setStatusCancelled();
        }

        if ($response->orderState == SberSbpOrderStatus::REFUNDED->value) {
            $paymentData->setStatusReturned();
        }

        if ($response->orderState == SberSbpOrderStatus::EXPIRED->value) {
            $paymentData->setStatusTimeout();
        }
    }
}
