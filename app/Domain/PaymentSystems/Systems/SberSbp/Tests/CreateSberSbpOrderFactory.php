<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp\Tests;

use App\Domain\PaymentSystems\Data\SberSbp\CreateSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOrderStatus;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateSberSbpOrderFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->uuid(),
            'order_form_url' => $this->faker->url(),
            'error_code' => '000000',
            'error_description' => $this->faker->text(20),
            'order_number' => $this->faker->uuid(),
            'order_state' => SberSbpOrderStatus::CREATED->value,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): CreateSberSbpOrderResponse
    {
        return new CreateSberSbpOrderResponse($this->makeArray($extra));
    }
}
