<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp\Tests;

use App\Domain\PaymentSystems\Data\SberSbp\RevokeSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOrderStatus;
use Ensi\LaravelTestFactories\BaseApiFactory;

class RevokeSberSbpOrderFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->uuid(),
            'order_state' => SberSbpOrderStatus::REVOKED->value,
            'error_code' => '000000',
            'error_description' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): RevokeSberSbpOrderResponse
    {
        return new RevokeSberSbpOrderResponse($this->makeArray($extra));
    }
}
