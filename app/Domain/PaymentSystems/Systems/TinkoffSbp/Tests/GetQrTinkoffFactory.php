<?php

namespace App\Domain\PaymentSystems\Systems\TinkoffSbp\Tests;

use App\Domain\PaymentSystems\Data\Tinkoff\GetQrTinkoffResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class GetQrTinkoffFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'OrderId' => $this->faker->modelId(),
            'PaymentId' => $this->faker->uuid(),
            'Data' => $this->faker->url(),
            'Success' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): GetQrTinkoffResponse
    {
        return new GetQrTinkoffResponse($this->makeArray($extra));
    }
}
