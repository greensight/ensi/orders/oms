<?php

use App\Console\Commands\Orders\CheckOrdersPaymentStatusCommand;
use App\Domain\Common\Models\Setting;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\Sber\Tests\GetOrderStatusExtendedFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;

use Tests\ComponentTestCase;
use Voronkovich\SberbankAcquiring\OrderStatus as SberOrderStatus;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command orders:cancel-expired-payments success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */
    /** @var Order $orderCancel */
    $orderCancel = Order::factory()->withPaymentStart()->create([
        'payment_expires_at' => now()->subDay(),
    ]);
    /** @var Order $orderOk */
    $orderOk = Order::factory()->withoutPaymentStart()->create();

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:cancel-expired-payments");

    assertDatabaseHas(Order::class, [
        'id' => $orderCancel->id,
        'status' => OrderStatusEnum::CANCELED,
    ]);
    assertDatabaseHas(Order::class, [
        'id' => $orderOk->id,
        'status' => OrderStatusEnum::WAIT_PAY,
    ]);
})->with(FakerProvider::$optionalDataset);

test("Command orders:mark-expired success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */
    $minutes = (int)Setting::getValue(SettingCodeEnum::PROCESSING);

    /** @var Order $orderExpired */
    $orderExpired = Order::factory()->withPaymentStart()->create([
        'created_at' => Date::now()->subMinutes($minutes)->subSecond()->format('Y-m-d H:i:s'),
        'status' => OrderStatusEnum::NEW,
        'is_expired' => false,
    ]);
    /** @var Order $orderOk */
    $orderOk = Order::factory()->withoutPaymentStart()->create([
        'created_at' => Date::now(),
        'status' => OrderStatusEnum::NEW,
        'is_expired' => false,
    ]);

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan("orders:mark-expired");

    assertDatabaseHas(Order::class, [
        'id' => $orderExpired->id,
        'is_expired' => true,
    ]);
    assertDatabaseHas(Order::class, [
        'id' => $orderOk->id,
        'is_expired' => false,
    ]);
})->with(FakerProvider::$optionalDataset);

test('Command orders:check-payment-status sber pay success', function () {
    /** @var ComponentTestCase $this */

    /** @var Order $orderToHold */
    $orderToHold = Order::factory()->withoutPaymentStart()->withSber()->create();
    /** @var Order $orderToPaid */
    $orderToPaid = Order::factory()->withHolded()->withSber()->create();
    /** @var Order $orderStayHold */
    $orderStayHold = Order::factory()->withHolded()->withSber()->create([
        'price' => 500,
        'payed_price' => 300,
    ]);

    $this->mockSberApi()
        ->shouldReceive('getOrderStatus')
        ->times(3)
        ->andReturnUsing(function ($orderId) use ($orderToHold, $orderToPaid, $orderStayHold) {
            $orderStatus = match ($orderId) {
                $orderToHold->payment_external_id => SberOrderStatus::APPROVED,
                $orderToPaid->payment_external_id => SberOrderStatus::DEPOSITED,
                $orderStayHold->payment_external_id => SberOrderStatus::APPROVED,
                default => null,
            };

            return GetOrderStatusExtendedFactory::new()->make(['orderStatus' => $orderStatus]);
        });

    $this->mock(SendOrderEventAction::class)->shouldReceive('execute');

    artisan(CheckOrdersPaymentStatusCommand::class);

    assertDatabaseHas(Order::class, [
        'id' => $orderToHold->id,
        'payment_status' => PaymentStatusEnum::HOLD,
        'payed_price' => $orderToHold->price,
    ]);

    assertDatabaseHas(Order::class, [
        'id' => $orderToPaid->id,
        'payment_status' => PaymentStatusEnum::PAID,
    ]);

    assertDatabaseHas(Order::class, [
        'id' => $orderStayHold->id,
        'payment_status' => PaymentStatusEnum::HOLD,
        'payed_price' => $orderStayHold->payed_price,
    ]);
});
