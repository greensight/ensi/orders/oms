<?php

namespace App\Console\Commands\Orders;

use App\Domain\Orders\Actions\Order\CheckOrdersPaymentStatusAction;
use Illuminate\Console\Command;

class CheckOrdersPaymentStatusCommand extends Command
{
    protected $signature = 'orders:check-payment-status';
    protected $description = 'Check and update payment status';

    public function handle(CheckOrdersPaymentStatusAction $action): void
    {
        $action->execute();
    }
}
