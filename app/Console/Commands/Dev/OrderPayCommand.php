<?php

namespace App\Console\Commands\Dev;

use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;

class OrderPayCommand extends Command
{
    protected $signature = 'orders:order-pay {orderId}';
    protected $description = 'Команда для тестирования оплаты заказа';

    public function handle(SaveOrderAction $saveOrderAction): void
    {
        if (in_production()) {
            $this->error('Команду нельзя выполнять в боевом окружении');

            return;
        }
        $orderId = $this->argument('orderId');

        /** @var Order|null $order */
        $order = Order::query()
            ->where('id', $orderId)
            ->first();
        if (!$order) {
            $this->error("Заказ с id={$orderId} не найден");

            return;
        }

        $order->payment_status = PaymentStatusEnum::PAID;
        $order->payed_price = $order->price;
        $order->payed_at = Date::now();
        $saveOrderAction->execute($order);
    }
}
