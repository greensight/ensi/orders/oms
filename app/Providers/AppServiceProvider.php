<?php

namespace App\Providers;

use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderFile;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Models\Shipment;
use App\Domain\Orders\Observers\DeliveryObserver;
use App\Domain\Orders\Observers\OrderFileObserver;
use App\Domain\Orders\Observers\OrderItemObserver;
use App\Domain\Orders\Observers\OrderObserver;
use App\Domain\Orders\Observers\ShipmentObserver;
use App\Domain\Refunds\Models\RefundFile;
use App\Domain\Refunds\Observers\RefundFileObserver;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        QueryBuilderRequest::setFilterArrayValueDelimiter('');
        $this->addObservers();
    }

    protected function addObservers(): void
    {
        OrderItem::observe(OrderItemObserver::class);
        OrderFile::observe(OrderFileObserver::class);
        RefundFile::observe(RefundFileObserver::class);

        Order::observe(OrderObserver::class);

        Delivery::observe(DeliveryObserver::class);
        Shipment::observe(ShipmentObserver::class);
    }
}
