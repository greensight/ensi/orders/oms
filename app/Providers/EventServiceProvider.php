<?php

namespace App\Providers;

use App\Domain\Orders\Events\DeliveryStatusUpdated;
use App\Domain\Orders\Events\OrderPaymentUpdated;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Domain\Orders\Events\ShipmentStatusUpdated;
use App\Domain\Orders\Listeners\CalcOrderIsRefundListener;
use App\Domain\Orders\Listeners\CancelChildrenFromOrderListener;
use App\Domain\Orders\Listeners\CancelPaymentWhenOrderCanceledListener;
use App\Domain\Orders\Listeners\DonePaymentWhenOrderDoneListener;
use App\Domain\Orders\Listeners\ReinitPaymentListener;
use App\Domain\Orders\Listeners\UpdateDeliveryStatusFromShipmentListener;
use App\Domain\Orders\Listeners\UpdateOrderStatusFromDeliveryListener;
use App\Domain\Refunds\Events\RefundStatusSaving;
use App\Domain\Refunds\Listeners\RefundPaymentWhenRefundApprove;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        ShipmentStatusUpdated::class => [
            UpdateDeliveryStatusFromShipmentListener::class,
        ],
        DeliveryStatusUpdated::class => [
            UpdateOrderStatusFromDeliveryListener::class,
        ],
        OrderStatusUpdated::class => [
            DonePaymentWhenOrderDoneListener::class,
            CancelChildrenFromOrderListener::class,
            CancelPaymentWhenOrderCanceledListener::class,
        ],
        RefundStatusSaving::class => [
            CalcOrderIsRefundListener::class,
            RefundPaymentWhenRefundApprove::class,
        ],
        OrderPaymentUpdated::class => [
            ReinitPaymentListener::class,
        ],
    ];

    public function boot(): void
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
