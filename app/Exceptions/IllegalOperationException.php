<?php

namespace App\Exceptions;

use LogicException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class IllegalOperationException extends LogicException implements HttpExceptionInterface
{
    public function getStatusCode(): int
    {
        return 400;
    }

    public function getHeaders(): array
    {
        return [];
    }
}
