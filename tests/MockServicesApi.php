<?php

namespace Tests;

use App\Domain\PaymentSystems\Systems\SberSbp\SberSbpClient;
use App\Domain\PaymentSystems\Systems\Tinkoff\TinkoffClient;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;
use Voronkovich\SberbankAcquiring\Client as SberApi;

trait MockServicesApi
{
    // =============== Internal Services ===============

    // =============== Catalog ===============
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    protected function mockOffersStocksApi(): MockInterface|StocksApi
    {
        return $this->mock(StocksApi::class);
    }

    // =============== External Services ===============

    // =============== Sber ===============
    protected function mockSberApi(): MockInterface|SberApi
    {
        $mock = $this->mock(SberApi::class);
        app()->bind(SberApi::class, fn () => $mock);

        return $mock;
    }

    protected function mockSberSbpApi(): MockInterface|SberSbpClient
    {
        return $this->mock(SberSbpClient::class);
    }

    // =============== Tinkoff ===============
    protected function mockTinkoffApi(): MockInterface|TinkoffClient
    {
        return $this->mock(TinkoffClient::class);
    }
}
